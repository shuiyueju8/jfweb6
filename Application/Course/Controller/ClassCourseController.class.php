<?php
namespace Course\Controller;
use Think\Controller;


/**
 * 学生课程管理控制器
 * 主要为学生课程数据处理
 * aClassInfo当前班级信息保存在cookie中为二维数组
 */
class ClassCourseController extends Controller
{
    protected function _initialize()
    {
        if(!is_administrator()){
            $this->error('非管理员，不能操作此模块',U('/Home/Index'));
        }


        /**
         * [获取班级列表 ]
         * @var [array]
         */
        $this->courseModel=D('Course/CourseXinxijishu');

        $this->classModel=D('Course/JfClass');
        $classlist=$this->classModel->order('bj_code','asc')->where($map)->select();
        $this->assign('classlist',$classlist);

        //检测班级是否设置
        $aClassInfo = cookie('aClassInfo');
        if(is_array($aClassInfo)){
        	$bj_code=$aClassInfo['bj_code'];
            // dump($aClassInfo);die;
        }else{
          $aClassInfo=$this->classModel->where('cur = 1')->field('create_time,update_time',true)->find();
          cookie('aClassInfo',$aClassInfo,3600000);//设置传递默认班级
        }
        

         $sub_menu['left']= array(
            array(
                'tab' => 'home', 'title' => $aClass['bj_name'],
                'href' =>  U('ClassCourse/setcurclass','bj_code='.$bj_code)
                ),
           array(
            'tab'=>'kecheng','title'=>$aClass['title'],
            'href'=>U('ClassCourse/selectBjCourse','bj_code='.$bj_code)
            )
           );
        $this->assign('sub_menu', $sub_menu);
        $this->assign('now_tab',ACTION_NAME);
        $this->assign('now_nav', 'course');
        $this->aClassInfo=$aClassInfo;
        //$this->assign('aClassInfo', $aClassInfo);
        
    }


   //教师管理首页
    public function index()
    {

     
     // $bj_code=I('bj_code',$this->aClassInfo['bj_code']);
     // $classmodel=$this->classModel;
     // $map=array('bj_code'=>$bj_code);
     // $aClass=$classmodel->where($map)->find();  
           
     //    $bjlist=$classmodel->order('bj_code','asc')->select();
     //     // dump($bjlist);die;
     //    //$this->assign('curclass', $aClass);
      
     //    // $this->assign('current', 'Course');        
        $this->display();

    }
       //设置当前班级
        public function setCurClass()
    {
        $bj_code=cookie('bj_code')?cookie('bj_code'):D('Member')->field('bj_code')->find(is_login());

        
             $map=array('cur'=>1);
             $aClass=$this->classModel->where($map)->find();
       
        $this->assign('aClassInfo', $aClass);        
        $this->assign('current', 'home');        
        $this->display();  
    }

    /*执行设置当前班级*/
    public function dosetCurClass()
    {

               $bj_code=I('bj_code',cookie("bj_code"));
               $map=array('bj_code'=>$bj_code);           
               
               $aClassInfo=cookie('aClassInfo');

             if($aClassInfo['bj_code']!=$bj_code)
	             {

	                $memberModel=D('Member');
	                $res=$memberModel->where(array('uid'=>is_login()))->setField('bj_code',$bj_code);


	                if($res)
	                {   
	                	$this->classModel->where('cur = 1')->setField('cur',0);
		                $this->classModel->where(array('bj_code'=>$bj_code))->setField('cur',1);
		                $aClassInfo=D('JFClass')->where('cur = 1')->field('create_time, update_time, week, cur',true)->find();
		                cookie('aClassInfo',$aClassInfo,360000);

		                //设置当前班级课程号		                
		                if($aClassInfo['course_id']) {cookie('cid',$aClassInfo['course_id'],36000);}
		                cookie('grade', $aClassInfo['grade'], 36000);//设置年级cookie	
		                $msg = '班级切换成功！';                
	                }

	                }else{
	                $msg="班级没有变化！"	;
	            } 
	         $this->success($msg, U('ClassCourse/index'));    

      
    }
    
    public function selectBjCourse(){
          $classModel=$this->classModel;
          $bj_code=I('bj_code',cookie('bj_code')); 
         if(IS_POST)
         {

          $id=I('id',0);
          $course_id=I('course_id', $this->aClassInfo['course_id']);
          $aCourse=$this->courseModel->find($course_id);//查询课程   

            $data=array(
                'id'   =>$id,
                'course_id'  =>$aCourse['id'],
                'title'=>$aCourse['title'],
                'up_cat'=>$aCourse['up_cat'],
                'allow_ext'=>$aCourse['allow_ext'],
                'teacher'  =>I('teacher'),
                'grade'=>I('grade'),
                'week' =>I('week')
                );
            $data=$classModel->create($data);

            if ($data) {
               
                    $res = $classModel->save($data);
                    if ($res !== false) {
                        $this->success('数据更新成功！',U('Course/ClassCourse/index'));
                    } else {
                        $this->error("没有更新任何数据!");
                    }
            } else {
                $this->error($classModel->getError());
            } 
         }else{
             //       
             
             $map=array('bj_code'=>$bj_code);
             
             $aClass=$classModel->where($map)->find();
            
             $id = $class['id'];
             $this->assign('id',$id);
             $xinximap['grade']=$aClass['grade'];
             $xinxi=$this->courseModel->where($xinximap)
                         ->field('id, title')
                         ->select();
             //dump($aClass);die;
             $this->assign('xinxi',$xinxi);
             $this->assign('aClass', $aClass);           
             $this->display();
         } 

        
    }
    
    //课程管理首页
    public function course()
    {
        $id=I('id');
        $course_grade=I('grade')?I('grade'):(cookie('grade')?cookie('grade'):'5A'); 
        // dump($grade);die;
        if(cookie("grade")!==$grade) {
            cookie("grade", $grade, 36000);
            }            
        $map=array('grade'=>$course_grade);

        $courseModel=$this->courseModel;
        $list=$courseModel->where($map)->field('uid',true) ->order(array('sort'=>'asc','id'=>'asc'))->select();
        $aCourse=$courseModel->find('id');

        $gradelist=M('JfGrade')->order(array('sort'=>'asc'))->select();

        $this->thistitle= '课程管理';
        $this->assign('gradelist',$gradelist);
        $this->assign('list',$list);
        $this->assign('acourse',$aCourse);
        $this->assign('course_grade',$course_grade);
        $this->display();
    }
        public function editCourse()
    {
        $id=I('id');
        $courseModel=$this->courseModel;//D('JfXinxijishu');
        $aCourse=$courseModel->find($id);
        $map=array('grade'=>$aCourse['grade']);
        $aGrade=M('JfGrade')->where($map)->find();
        $gradelist=M('JfGrade')->order(array('sort'=>'asc'))->select();
        //dump($list);
        //dump($gradelist);
        $this->thistitle= '编辑课程';
        $this->assign('gradelist',$gradelist);
        //$this->assign('list',$list);
        $this->assign('acourse',$aCourse);
        $this->assign('agrade',$aGrade);
        $this->display();
    }
        //do新增课程信息
    public function saveCourse()
    {
        $data=I('post.');
        $data['id']=(int)I('post.id');
        // dump($data);
        
        $courseModel=$this->courseModel;
        $data=$courseModel->create($data);
         // dump($data);die;
        $res=$courseModel->save($data);
        if ($res !== false) {
            $this->success('课程信息修改成功！',U('Course/ClassCourse/course'));
            } else {
                $this->error("修改失败，没有更新任何数据!", U('Course/ClassCourse/course'));
            }

    }
    //do新增课程信息
    public function doAddCourse()
    {
        $data=I('post.');
        $data['id']=(int)I('post.id');
        dump($data);
        
        $courseModel=$this->courseModel;
        $data=$courseModel->create($data);
         // dump($data);die;
        $res=$courseModel->add($data);
        if ($res !== false) {
            $this->success('课程信息新增成功！',U('Course/ClassCourse/course'));
            } else {
                $this->error("新增失败，没有更新任何数据!", U('Course/ClassCourse/course'));
            }

    }
    
    //添加课程内容
    public function coursecontent()
    {
        //dump(cookie('cid'));

        $cid=I('cid',cookie('cid'));//cookie('cid')
        $map['cid']=$cid;
        $coursemodel=D('JfCourseContent');  
        $order=array('sort'=>'asc','create_time'=>'desc');   
        $contents=$coursemodel->where($map)->order($order)->select();
       // dump($contents);
        $this->assign('contents',$contents);
        $this->assign('cid',$cid);
         if(IS_POST){
            $data=I('post.');
            $data['create_time']=NOW_TIME;
            $data =  $coursemodel->create($data);
            // dump($data);die;
            $res =$coursemodel->add($data);
            if ($res !== false) {
            $this->success('新增课程内容成功！',U('Course/ClassCourse/coursecontent',array('cid'=>$cid)));
            } else {
                $this->error("新增失败，没有更新任何数据!");
            }
        }
        $this->display();
    }

    //编辑课程内容
    public function editcoursecontent()
    {
        $id=I('get.id',0);//cookie('cid')课程内容id
        $map['id']=$id;
        $coursemodel=D('JfCourseContent');     
        $content=$coursemodel->find($id);
        $this->assign('acontent',$content);
        $this->assign('id',$id);
        if(IS_POST){
            $data=I('post.');
            $data['create_time']=NOW_TIME;
            $coursemodel->create($data);
            $res =$coursemodel->save();
            if ($res !== false) {
            $this->success('数据更新成功！',U('Course/ClassCourse/coursecontent'));
            } else {
                $this->error("没有更新任何数据!");
            }
        }
        $this->display();
    }



}