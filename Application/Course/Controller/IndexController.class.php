<?php
namespace Course\Controller;
use Think\Controller;

class IndexController extends Controller
{
    /**
     * 学生课程浏览
     * @var
     */
    public function _initialize()
    {
        
        // $this->classModel=D('Course/JfClass');        
        // $bj_code= I('bj_code',cookie('bj_code'));
        // if(!$bj_code){
        // 	$bj_code=$this->classModel->where(array('cur'=>1))->getField('bj_code');
        // 	cookie('bj_code',$bj_code,360000);
        // }
        $aClassInfo=cookie('aClassInfo');
        
        if(!empty($aClassInfo['course_id'])){
            $this->aClassInfo=$aClassInfo;
        	$map=['id'=>$aClassInfo['course_id']];
        	$acousre=M('CourseXinxijishu')->where($map)->find();
        	// dump($acousre);
        	$leftmenu[]=['tab' => 'index', 'title' => $acousre['grade_t'].'-'.$acousre['title'], 'href' => U('course/index/index') ,
        	             'icon'=>'book'];
        }
        // if(is_administrator()){
        // 	$leftmenu[]=['tab' => 'home', 'title' => '班级管理', 'href' => U('course/ClassCourse/index')];
        // }


        
        $sub_menu = ['left'=>$leftmenu];

        /**右侧菜单*/

        if(is_login()){
        	if(is_administrator()){
        		$sub_menu['right'][]=['tab' => 'home', 'title' => '班级管理', 'href' => U('course/ClassCourse/index')];
        	}
        	$sub_menu['right'][]=['tab' => 'post', 'title' => '上传作品', 'href' => '#frm-post-popup','a_class'=>'open-popup-link'];
        }
        

        // if (check_auth('addVideoContent')) {
        //     $sub_menu['right'] = array(
        //         array()
        //     );
        // }
        // foreach ($tree as $cat) {
        //     if ($cat['_']) {
        //         $children = array();
        //         $children[] = array('tab' => 'cat_' . $cat['id'], 'title' => '全部', 'href' => U('course/index/index', array('video_id' => $cat['id'])));
        //         foreach ($cat['_'] as $child) {
        //             $children[] = array('tab' => 'cat_' . $cat['id'], 'title' => $child['title'], 'href' => U('course/index/index', array('video_id' => $child['id'])));
        //         }

        //     }
        //     $menu_item = array('children' => $children, 'tab' => 'cat_' . $cat['id'], 'title' => $cat['title'], 'href' => U('blog/article/lists', array('category' => $cat['id'])));
        //     $sub_menu['left'][] = $menu_item;
        //     unset($children);
        // }
        $this->assign('sub_menu', $sub_menu);
        $this->assign('now_tab',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);

    }

    public function index()
    {
        $user_info=query_user(array('nickname'));

        $cid=I('cid',$this->aClassInfo['course_id']);
        if(cookie('cid')!==$cid){
            cookie('cid',$cid, 864000);
        }
       
        $map['cid']=$cid;//92;//
        // $map['status'] = 1;
        $order=array('sort'=>'asc','create_time'=>'desc');
        $course_content=M('CourseContent')->where($map)->order($order)->select();

        

        // 如果没资源有课程相关退回首页
        if(!$course_content){
            $course_content[0]=array(
                'title'=>' 出错了',
                'content'=>'<p><span style="font-size: 18px;">找不到可用的课程资源，可能是您任教的老师还未上传相关资料。</span><br/>' .
               '<a href="'.U('Home/Index/index').'"><span style="font-size: 20px; color: red;">返回首页</span></a> 
                </p>'
                );

        }
        $this->assign('contents', $course_content);
        $keti=M('CourseXinxijishu')->field('id,title')->where(array('id'=>$cid))->select();
        // dump($keti);die;
        // $this->assign('bj_code', $bj_code);
       
         $this->assign('keti',$keti);
         $this->assign('aContent','作者：'.$this->aClassInfo['bj_name'].'  '.$user_info['nickname']);
         $this->assign('course_id', $course_id);
         $this->setTitle('课程');
         $this->assign('current','index');
         $this->display();
    }


}