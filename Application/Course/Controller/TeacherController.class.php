<?php
namespace Course\Controller;
use Think\Controller;


/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class TeacherController extends Controller
{
    protected function _initialize()
    {   
        // $adminid=is_administrator();
        if(!is_administrator()){
            $this->error('非管理员，不能操作此模块',U('/Home/Index'));
        }
        // cookie('bj_code',null);
        if(is_login()){
        if(!cookie('bj_code')) {
            cookie("bj_code", D('Member')->field('bj_code')->find(is_login()), 36000);//设置传递默认班级
            // $this->success('更新成功',U('index'));
          }
        }
        $this->classModel=D('Course/JfClass');
        $classlist=$this->classModel->field('status',true)->order('bj_code','asc')->where($map)->select();
        $this->assign('classlist',$classlist);
        $this->xinxiModel=D('Course/JfXinxijishu');
        //设置导航条信息

         $bj_code=I('bj_code',cookie('bj_code'));

         $map=array('bj_code'=>$bj_code);
         $aClass=$this->classModel->where($map)->find();
         $sub_menu['left']= array(
            array(
                'tab' => 'home', 'title' => $aClass['bj_name'],
                'href' =>  U('ClassCourse/setcurclass','bj_code='.$bj_code)
                ),
           array(
            'tab'=>'kecheng','title'=>$aClass['title'],
            'href'=>U('ClassCourse/selectBjCourse','bj_code='.$bj_code)
            )
           );
        $this->assign('sub_menu', $sub_menu);
        $this->assign('bj_code',$bj_code);
        $this->assign('now_tab',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
        $cur_bj=cookie('cur_bj');
        cookie('cur',$cur_bj,3600000);
        $this->assign('cur', $cur_bj);
       
    }
    //教师管理首页
    public function index()
    {
     redirect(U('ClassCourse/index'), 0, '页面跳转中...');
     $bj_code=I('bj_code',cookie('bj_code'));
     $classmodel=$this->classModel;
     $map=array('bj_code'=>$bj_code);
     $aClass=$classmodel->where($map)->find();  
           
        $bjlist=$classmodel->order('bj_code','asc')->select();
        // dump($bjlist);die;
        $this->assign('curclass', $aClass);
      
        $this->assign('current', 'Course');        
        $this->display();

    }
    /*学生批量重置密码*/
    public function initpassword(){
    	$cur['bj_code']=$bj_code=I('bj_code',cookie('bj_code'));
    	if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
    		cookie("bj_code", $bj_code, 36000);
    	}
    	$map['status']=1;

    	
        $classlist=D('JfClass')->field('id,bj_code,bj_name')->order('bj_code','asc')->where($map)->select();

        $studentmodel=D('Member');
        $map2=array(
        	'status'=>1,
        	'uid'=>array('not in',str2arr(C('USER_ADMINISTRATOR'))),
        	'bj_code'=>$bj_code
        	);
        //排除管理员账号
        $list=$studentmodel->where($map2) ->order(array('st_code'=>'asc'))->select();
        $this->thistitle= '学生密码重置' ;     
        $this->assign('classlist',$classlist);
        $this->assign('list',$list);
        $this->assign('cur',$cur); 

    	$this->display();

    }
    /*执行修改密码*/
    public function doinitpass(){
             $uids = I('id');
             // dump($uids);die;
            !is_array($uids) && $uids = explode(',', $uids);
            foreach ($uids as $key => $val) {
                if (!query_user('uid', $val)) {
                    unset($uids[$key]);
                }
            }
            if (!count($uids)) {
                $this->error('没有用户被选中，请先选择用户！');//L('_ERROR_USER_RESET_SELECT_').L('_EXCLAMATION_')
            }
            $ucModel = UCenterMember();
            $data['password']=think_ucenter_md5('2018', UC_AUTH_KEY);

            $res = $ucModel->where(array('id' => array('in', $uids)))->save(array('password' => $data['password']));
            if ($res) {
                $this->success('密码重置成功，'.$res);//L('_SUCCESS_PW_RESET_').L('_EXCLAMATION_')
            } else {
                $this->error('密码重置失败，原密码可能是‘2018’的哦');
            }
    }

     //学生位置管理

    public function member()
    {
    	$cur['bj_code']=$bj_code=I('bj_code',cookie('bj_code'));
    	if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
    		cookie("bj_code", $bj_code, 36000);
    	}
    	$map['status']=1;

    	
        $classlist=D('JfClass')->field('id,bj_code,bj_name')->order('bj_code','asc')->where($map)->select();

        $studentmodel=D('Member');
        $map2=array(
        	'status'=>1,
        	'uid'=>array('not in',str2arr(C('USER_ADMINISTRATOR'))),
        	'bj_code'=>$bj_code
        	);
        //排除管理员账号
        $list=$studentmodel->where($map2) ->order(array('pc'=>'asc'))->select();
        $this->thistitle= '位置学生名单' ;     
        $this->assign('classlist',$classlist);
        $this->assign('list',$list);
        $this->assign('cur',$cur);    	
        $this->display();
    }
   /**清除学生信息*/
   public function cleanweizhi(){
        
            $uid=I('get.uid',0,'intval'); 
            $map=array('uid'=>$uid);
            $studentModel=D('Member');
            // dump($studentModel);            
            $res=$studentModel->where($map)->setField('pc','');
            // dump($data);//die;
            //$res =$studentModel->save($data);
            if ($res !== false) {
            $this->success('数据更新成功！',U('Course/Teacher/student',array('id'=>$data['id'])));
            } else {
                $this->error("没有更新任何数据!");
            }

    }
    /**编辑Member信息*/
        public function memberEdit()
    {

        $map['status']=1;        
        $classlist=D('JfClass')->field('id,bj_code,bj_name')->order('bj_code','asc')->where($map)->select();

        $aId=I('uid',0,'intval');
        $studentmodel=D('Member');    

        	$aData=$studentmodel->field('uid,bj_code,st_code,nickname,pc')->find($aId);
	        //dump($aData);die;
	        $this->assign('data',$aData);

	        $this->thistitle= '编辑学生位置' ; 

	        $this->assign('classlist',$classlist);    
	        $this->display();       
    }
    

    /**执行修改学生信息*/
   public function doEditMember(){
        
            $data=I('post.'); 
            $studentmodel=D('Member');          
            $data=$studentmodel->create($data);
            // dump($data);die;
            $res =$studentmodel->save($data);
            if ($res !== false) {
            $this->success('数据更新成功！',U('Course/Teacher/member',array('uid'=>$aId)));
            } else {
                $this->error("没有更新任何数据!");
            }

    }
        //学生管理

    public function student()
    {
    	$cur['bj_code']=$bj_code=I('bj_code',cookie('bj_code'));
    	if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
    		cookie("bj_code", $bj_code, 36000);
    	}
    	$map['status']=1;
    	
        $classlist=D('JfClass')->field('id,bj_code,bj_name')->order('bj_code','asc')->where($map)->select();

        $studentmodel=D('JfStudent');
        $map2=array(
        	'status'=>1,
        	'bj_code'=>$bj_code
        	);
        $list=$studentmodel->where($map2) ->order(array('id'=>'asc'))->select(); 
        // dump($list);die;
        $this->thistitle= '班级学生管理' ;
        //$this->assign('curtitle',$title);      
        $this->assign('classlist',$classlist);
        $this->assign('list',$list);
        $this->assign('cur',$cur);    	
        $this->display();
    }

        /**编辑学生信息*/
        public function studentEdit()
    {

        $map['status']=1;        
        $classlist=D('JfClass')->field('id,bj_code,bj_name')->order('bj_code','asc')->where($map)->select();

        $aId=I('id',0,'intval');
        $studentmodel=D('JfStudent');   

        	$aData=$studentmodel
        	// ->field('id,bj_code,bj_name,st_code,st_name,st_code,sex,uid')
        	->find($aId);
	        // dump($aData);die;
	        $this->assign('data',$aData);

	        $this->thistitle= '编辑学生位置' ; 
	        $this->assign('classlist',$classlist);    
	        $this->display();       
    }

   /**执行修改学生信息*/
   public function doEditStudent(){
        
            $data=I('post.'); 
            $studentModel=D('JfStudent');
            // dump($studentModel);            
            $data=$studentModel->create($data);
            // dump($data);//die;
            $res =$studentModel->save($data);
            if ($res !== false) {
            $this->success('数据更新成功！',U('Course/Teacher/student',array('id'=>$data['id'])));
            } else {
                $this->error("没有更新任何数据!");
            }

    }

    
    //查看未注册学生名单
        public function student0()
    {
    	$cur['bj_code']=$bj_code=I('bj_code',cookie('bj_code'));
    	if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
    		cookie("bj_code", $bj_code, 36000);
    	}

    	$map['status']=1;
    	
        $classlist=D('JfClass')//->field('id,bj_code,bj_name')
        ->order('bj_code','asc')->where($map)->select();

        $studentmodel=D('JfStudent');
        $map2=array(
        	'status'=>1,
        	'uid'=>0,
        	'bj_code'=>$bj_code
        	);

        $list=$studentmodel->where($map2) ->order(array('id'=>'asc'))->select();
        // dump($list);die;
        $this->thistitle= '未注册学生名单' ;     
        $this->assign('classlist',$classlist);
        $this->assign('list',$list);
        $this->assign('cur',$cur);    	
        $this->display();
    }

}