<?php
/**
 * Created by PhpStorm.
 * User: caipeichao
 * Date: 14-3-11
 * Time: PM5:41
 */

namespace Admin\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Builder\AdminTreeListBuilder;


class CourseController extends AdminController
{
    protected $CourseModel;

    function _initialize()
    {
        $this->CourseModel = D('Course/JfStdoc');
        $this->studentModel = D('Course/JfStudent');
        $this->classModel=D('Course/JfClass');
        $this->xinxijishuModel=D('Course/JfXinxijishu');
        parent::_initialize();
        include APP_PATH. 'Course/Common/function.php';//加载公共函数库
       
    }
    /** 信息技术课程管理 */
    public function xinxijishu()
    {
        $grade="5A";
        $map=array('grade'=>$grade);
        $order=array('grade'=>'asc','sort'=>'asc');
     $this->data=$this->xinxijishuModel->order($order)->where($map)->select();
     //dump($data);
     $this->assign('meta_title','信息技术课程管理');
     $this->display('Course@Admin/xinxijishu');
    }
    /*导入课程信息*/
    public function importXinxijishu()
    {
        $thisModel=$this->xinxijishuModel;
        $data=array();
        if(IS_POST){
        $config = array(
            'maxSize'    =>   30*1024*1024, // 设置附件上传大小
            'rootPath'   =>   './Uploads/',
            'savePath'   =>   'Tmp/',// 设置附件上传目录
            'saveName'   =>    array('uniqid'),//iconv('utf-8', 'gbk', $save),
            'exts'       =>    array('xls', 'xlsx'),// 设置附件上传类
            'autoSub'    =>    true,
            'subName'    =>    array('date','Y-m'),
            );
        $upload = new \Think\Upload($config);// 实例化上传类

        // 上传文件
        $info   =   $upload->uploadOne($_FILES['excelData']);      
        $filename = './Uploads/'.$info['savepath'].$info['savename'];
        $ext = $info['ext'];
        // print_r($info);exit;
        if(!$info) {// 上传错误提示错误信息
              $this->error($upload->getError());
          }else{
          // 上传成功
           $data=readexcel2arry($filename,$ext,3);//读取文件中的数据
            // dump($data);die;
            foreach ($data as $k=>$v){
                $data=array(
                    'grade'    =>  $v['B'],
                    'grade_t'  =>  $v['C'],
                    'title'    =>  $v['D'],
                    'up_cat'   =>  $v['E'],
                    'allow_ext'=>  $v['F'],
                    'sort'     =>  $v['G'],
                    'update_time'=> NOW_TIME,
                    'create_time'=> NOW_TIME,
                    'status'    => 1
                    );
                $map=array(
                    'grade'    =>  $v['B'],
                    'title'    =>  $v['D'],
                    'status'=>1
                    );
              
         // dump($data);dump($map);die;
                $result = $thisModel->where($map)->find();
                  

                if($result){
                    //更新操作
                    $data['update_time']=NOW_TIME;
                    $up_data = $thisModel->create($data);
                    $result = $thisModel->where($map)->save($up_data);
                    $update_num++;
                    //获取最后一次查询//dump($studentModel->_sql());
                     
                }else{
                    //入库操作                   
                    $adddata=$thisModel->create($stdata[$k]);//要实现自动增加自动验证需调用create函数 
                     // dump($adddata);  die;         
                    $result =$thisModel->add($adddata);
                    $add_num++;
                  
                }
         
        }
        $infotext='本次操作，共导入'.$add_num.'条数据！<br>更新'.$update_num.'条数据！';
        // dump($infotext);die;
       $this->success( $infotext, U('Admin/Course/xinxijishu'), 10);
           
        }
       }
    }
    /*导出课程信息Excel表格 */
    public function exportXinxijishu()
    {
        $order=array(
            'grade'=>'asc',
            'sort'=>'asc'
            );
        $listdata=$this->xinxijishuModel->field('grade,grade_t, title, up_cat, allow_ext, sort, update_time, create_time')
                       ->order($order)->select();
        //$data=listAddID($data);
      $width=array('5','6','6','26','6','12','5','12','auto','auto');
      $title=array('序号','册次','年级','标题','类别','上传文档类型','排序','更新日期','创建日期');

       if(!is_array($listdata)){
         $listdata[]=array('','本学年','暂无数据','','','');//没有数据设置无数据提示行防止溢出死机
       }else{
          $listdata=listAddID($listdata);//增加自动排序序号
       }

    $data[1]=array(
      'sheetname'=>'课程信息',
      'header'=>'课程信息',
      'title'=>$title,
      'width'=>$width,
      'data'=>$listdata,
      );
     $savefile=$year."学年课程信息汇总".date(md);
     // dump($data);
    // 输出文档函数在common/function.php中
    exportExcel($data,$savefile);
    }
/**导入班级 上传excel方法*/
        public function importClass(){

            
            if(IS_POST){
                $config = array(
                    'maxSize'    =>   30*1024*1024, // 设置附件上传大小
                    'rootPath'   => './Uploads/',
                    'savePath'   =>     'Tmp/',// 设置附件上传目录
                    'saveName'   =>   array('uniqid'),//iconv('utf-8', 'gbk', $save),
                    'exts'       =>    array('xls', 'xlsx'),// 设置附件上传类
                    'autoSub'    =>    true,
                    'subName'    =>    array('date','Y-m'),
                    );
                $upload = new \Think\Upload($config);// 实例化上传类

                // 上传文件
                $info   =   $upload->uploadOne($_FILES['excelData']);      
                $filename = './Uploads/'.$info['savepath'].$info['savename'];
                $exts = $info['ext'];
                // print_r($info);exit;
                if(!$info) {// 上传错误提示错误信息
                      $this->error($upload->getError());
                  }else{
                  // 上传成功
                   $this->read_class($filename, $exts);
                }

            }else{
            	$map = array(                  
                    'status'=>1); 
            	$classnum=$this->classModel->where($map)->count();
            	$this->assign('classnum',$classnum);
                $this->assign('meta_title','导入班级信息');
                $this->display('Course@Admin/import_class');
            }

        }
        


    /*导入数据方法*/
    protected function read_class($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        vendor("PHPExcel"); 
        //创建PHPExcel对象，注意，不能少了\ 
        $objPHPExcel = new \PHPExcel();  
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            $objReader = \PHPExcel_IOFactory::createReader('Excel5'); 

        }else if($exts == 'xlsx'){
            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        }
        $objReader->setReadDataOnly(true);   
        //载入文件
        $objPHPExcel = $objReader->load($filename); 
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$objPHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=4;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //读取到的数据，保存到数组$arr中
                $cell=$currentSheet->getCell($address)->getValue();                
                if($cell instanceof PHPExcel_RichText){//富文本转换字符串
                    $cell  = $cell->__toString();
                }
                $data[$currentRow][$currentColumn]=$cell;
                //print_r($cell);
            } 
        }
          // dump($data);die;
        $this->save_class($data);
    }

    /*保存导入数据*/
    public function save_class($data)
    {
           // dump($data);exit;

         // dump(get_bjname('201201'));
         // $bj_code=get_bjcode('四年级1班');
         // dump($bj_code);die;

 
        $thisModel = $this->classModel;
        $update_num=0;
        $add_num=0;
        $client_ip=get_client_ip(1);
        foreach ($data as $k=>$v){

        	$courseMap=array(
        		'grade'=>$v['E'],
        		'status'=>1
        		);
        	// dump($courseMap);
        	$course=M('JfXinxijishu')->where($courseMap)->find();//
        	// dump($course);die;

        


             $stdata[$k]=array(
             	    'bj_code'=>$v['B'],
             	    'bj_name'=>$v['C'],
                    'grade'=>$v['D'],
                    'course_grade'=>$v['E'],
                    'teacher'=>$v['F'],
                    'sort'=>$v['G'],                    
                    'lid'=>$course['id'],
                    'title'=>$course['title'],
                    'up_cat'=>$course['up_cat'],
                    'allow_ext'=>$course['allow_ext'],
                    // 'update_ip'=>$client_ip,
                    'status'=>1
                    ); 

                $map = array(
                    'bj_name'=>$v['C'],
                    'grade'=>$v['D'],                                 
                    'status'=>1); 
                    //dump($map)  ;         
                $result = $thisModel->where($map)->find();
                  

                if($result){
                    //更新操作
                    $data[$k]['update_time']=NOW_TIME;
                    $up_data = $thisModel->create($data[$k]);
                    $result = $thisModel->where($map)->save($up_data);
                    $update_num++;
                    //获取最后一次查询//dump($studentModel->_sql());
                     
                }else{
                    //入库操作                   
                    $adddata=$thisModel->create($stdata[$k]);//要实现自动增加自动验证需调用create函数 
                    // dump($adddata);           
                    $result = $thisModel->add($adddata);
                    $add_num++;
                  
                }
         
        }
        $infotext='本次操作，共导入'.$add_num.'条班级数据！<br>更新'.$update_num.'条班级数据！';
        // dump($infotext);die;
       $this->success( $infotext, U('Admin/Course/importClass'), 10);

    }


     /**导入学生名单 上传excel方法*/
        public function importStudent(){

            
            if(IS_POST){
                $config = array(
                    'maxSize'    =>   30*1024*1024, // 设置附件上传大小
                    'rootPath'   => './Uploads/',
                    'savePath'   =>     'Tmp/',// 设置附件上传目录
                    'saveName'   =>   array('uniqid'),//iconv('utf-8', 'gbk', $save),
                    'exts'       =>    array('xls', 'xlsx'),// 设置附件上传类
                    'autoSub'    =>    true,
                    'subName'    =>    array('date','Y-m'),
                    );
                $upload = new \Think\Upload($config);// 实例化上传类

                // 上传文件
                $info   =   $upload->uploadOne($_FILES['excelData']);      
                $filename = './Uploads/'.$info['savepath'].$info['savename'];
                $exts = $info['ext'];
                // print_r($info);exit;
                if(!$info) {// 上传错误提示错误信息
                      $this->error($upload->getError());
                  }else{
                  // 上传成功
                   $this->student_import($filename, $exts);
                }

            }else{
                $this->assign('meta_title','导入学生信息');
                $this->display('Course@Admin/import_student');
            }

        }
        


    /*导入数据方法*/
    protected function student_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        vendor("PHPExcel"); 
        //创建PHPExcel对象，注意，不能少了\ 
        $objPHPExcel = new \PHPExcel();  
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            $objReader = \PHPExcel_IOFactory::createReader('Excel5'); 

        }else if($exts == 'xlsx'){
            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        }
        $objReader->setReadDataOnly(true);   
        //载入文件
        $objPHPExcel = $objReader->load($filename); 
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$objPHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=4;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //读取到的数据，保存到数组$arr中
                $cell=$currentSheet->getCell($address)->getValue();                
                if($cell instanceof PHPExcel_RichText){//富文本转换字符串
                    $cell  = $cell->__toString();
                }
                $data[$currentRow][$currentColumn]=$cell;
                //print_r($cell);
            } 
        }
        // dump($data);die;
        $this->save_import($data);
    }

    /*保存导入数据*/
    protected function save_import($data)
    {
          // dump($data);exit;

         // dump(get_bjname('201201'));
         // $bj_code=get_bjcode('四年级1班');
         // dump($bj_code);die;

 
        $studentModel = $this->studentModel;
        $update_num=0;
        $add_num=0;
        $client_ip=get_client_ip(1);
        foreach ($data as $k=>$v){
        	$bj_code=get_bjcode($v['A']);
        	$st_code=$bj_code*100 + $v['C'];


             $stdata[$k]=array(

                    'bj_name'=>$v['A'],
                    'grade'=>$v['B'],
                    'code'=>$v['C'],
                    'bj_code'=>$bj_code,
                    'st_code'=>$st_code,
                    'st_name'=>$v['D'],                    
                    'sex'=>$v['E'],
                    'bz'=>$v['F'],
                    'uid'=> 0 ,
                    // 'update_ip'=>$client_ip,
                    'status'=>1
                    ); 

                $map = array(
                    'bj_name'=>$v['A'],
                    'grade'=>$v['B'],
                    //'code'=>$v['C'],
                    'st_name'=>$v['D'],                  
                    'status'=>1); 
                    //dump($map)  ;         
                $result = $studentModel->where($map)->find();
  

                if($result){
                    //更新操作
                    $stdata[$k]['update_time']=NOW_TIME;
                    $up_data = $studentModel->create($stdata[$k]);
                    $result = $studentModel->where($map)->save($up_data);
                    $update_num++;
                    //获取最后一次查询//dump($studentModel->_sql());
                     
                }else{
                    //入库操作                   
                    $adddata=$studentModel->create($stdata[$k]);//要实现自动增加自动验证需调用create函数 
                    // dump($adddata);die;           
                    $result = $studentModel->add($adddata);
                    $add_num++;
                  
                }
         
        }
        $infotext='本次操作，共导入'.$add_num.'条学生数据！<br>更新'.$update_num.'条学生数据！';
        // dump($infotext);die;
       $this->success( $infotext, U('Admin/Course/importstudent'), 10);

    }

    public function config()
    {
        $admin_config = new AdminConfigBuilder();
        $data = $admin_config->handleConfig();
        $data['NEED_VERIFY'] = $data['NEED_VERIFY'] ? $data['NEED_VERIFY'] : 0;
        $data['DISPLAY_TYPE'] = $data['DISPLAY_TYPE'] ? $data['DISPLAY_TYPE'] : 'list';
        $data['Course_SHOW_TITLE'] = $data['Course_SHOW_TITLE'] ? $data['Course_SHOW_TITLE'] : '最热视频';
        $data['Course_SHOW_COUNT'] = $data['Course_SHOW_COUNT'] ? $data['Course_SHOW_COUNT'] : 4;
        $data['Course_SHOW_ORDER_FIELD'] = $data['Course_SHOW_ORDER_FIELD'] ? $data['Course_SHOW_ORDER_FIELD'] : 'view_count';
        $data['Course_SHOW_ORDER_TYPE'] = $data['Course_SHOW_ORDER_TYPE'] ? $data['Course_SHOW_ORDER_TYPE'] : 'desc';
        $data['Course_SHOW_CACHE_TIME'] = $data['Course_SHOW_CACHE_TIME'] ? $data['Course_SHOW_CACHE_TIME'] : '600';
        $admin_config->title('课程基本设置')
            ->keyBool('NEED_VERIFY', '投稿是否需要审核', '默认无需审核')
            ->keyRadio('DISPLAY_TYPE', '默认展示形式', '前台列表默认以该形式展示',array('list'=>'列表','masonry'=>'瀑布流'))
            ->buttonSubmit('', '保存')->data($data);
        $admin_config->keyText('Course_SHOW_TITLE', '标题名称', '在首页展示块的标题');
        $admin_config->keyText('Course_SHOW_COUNT', '显示视频的个数', '只有在网站首页模块中启用了视频块之后才会显示');
        $admin_config->keyRadio('Course_SHOW_ORDER_FIELD', '排序值', '展示模块的数据排序方式', array('view_count' => '阅读数', 'reply_count' => '回复数', 'create_time' => '发表时间', 'update_time' => '更新时间'));
        $admin_config->keyRadio('Course_SHOW_ORDER_TYPE', '排序方式', '展示模块的数据排序方式', array('desc' => '倒序，从大到小', 'asc' => '正序，从小到大'));
        $admin_config->keyText('Course_SHOW_CACHE_TIME', '缓存时间', '默认600秒，以秒为单位');
        $admin_config->group('基本配置', 'NEED_VERIFY,DISPLAY_TYPE')->group('首页展示配置', 'Course_SHOW_COUNT,Course_SHOW_TITLE,Course_SHOW_ORDER_TYPE,Course_SHOW_ORDER_FIELD,Course_SHOW_CACHE_TIME');

        $admin_config->groupLocalComment('本地评论配置','CourseContent');
        $admin_config->display();
    }

    public function Course()
    {
        //显示页面
        $builder = new AdminTreeListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';
        $attr1 = $attr;
        $attr1['url'] = $builder->addUrlParam(U('setWeiboTop'), array('top' => 1));
        $attr0 = $attr;
        $attr0['url'] = $builder->addUrlParam(U('setWeiboTop'), array('top' => 0));
        $tree = D('Course/JfStdoc')->getTree(0, 'id,title,sort,pid,status');
        $builder->title('课程管理')
            ->buttonNew(U('Course/add'))
            ->data($tree)
            ->display();
    }

    public function classlist()
    {
        $map = array('status' => 1);
        $classModel = $this->classModel;
        $page=I('page',1);
        $r=I('r',30);
        $totalCount=$classModel->where($map)->count();
        if($totalCount){
            $list=$classModel->where($map)->order('sort','desc')->page($page, $r)->field($field)->select();
        }
        //dump($list);die;
        $this->assign('_list', $list);

        $this->meta_title = '班级列表';
        $this->display('Course@Admin/classlist');
        
    }

    public function add($id = 0, $pid = 0)
    {
        if (IS_POST) {
            if ($id != 0) {
                $Course = $this->CourseModel->create();
                if ($this->CourseModel->save($Course)) {
                    $this->success('编辑成功。');
                } else {
                    $this->error('编辑失败。');
                }
            } else {
                $Course = $this->CourseModel->create();
                if ($this->CourseModel->add($Course)) {

                    $this->success('新增成功。');
                } else {
                    $this->error('新增失败。');
                }
            }


        } else {
            $builder = new AdminConfigBuilder();
            $Courses = $this->CourseModel->select();
            $opt = array();
            foreach ($Courses as $Course) {
                $opt[$Course['id']] = $Course['title'];
            }
            if ($id != 0) {
                $Course = $this->CourseModel->find($id);
            } else {
                $Course = array('pid' => $pid, 'status' => 1);
            }


            $builder->title('新增分类')->keyId()->keyText('title', '标题')->keySelect('pid', '父分类', '选择父级分类', array('0' => '顶级分类') + $opt)
                ->keyStatus()->keyCreateTime()->keyUpdateTime()
                ->data($Course)
                ->buttonSubmit(U('Course/add'))->buttonBack()->display();
        }

    }

    public function CourseTrash($page = 1, $r = 20, $model = '')
    {
        $builder = new AdminListBuilder();
        $builder->clearTrash($model);
        //读取微博列表
        $map = array('status' => -1);
        $model = $this->CourseModel;
        $list = $model->where($map)->page($page, $r)->select();
        $totalCount = $model->where($map)->count();

        //显示页面

        $builder->title('视频回收站')
            ->setStatusUrl(U('setStatus'))->buttonRestore()->buttonClear('Course/Course')
            ->keyId()->keyText('title', '标题')->keyStatus()->keyCreateTime()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }

    public function operate($type = 'move', $from = 0)
    {
        $builder = new AdminConfigBuilder();
        $from = D('Course')->find($from);

        $opt = array();
        $Courses = $this->CourseModel->select();
        foreach ($Courses as $Course) {
            $opt[$Course['id']] = $Course['title'];
        }
        if ($type === 'move') {

            $builder->title('移动分类')->keyId()->keySelect('pid', '父分类', '选择父分类', $opt)->buttonSubmit(U('Course/add'))->buttonBack()->data($from)->display();
        } else {

            $builder->title('合并分类')->keyId()->keySelect('toid', '合并至的分类', '选择合并至的分类', $opt)->buttonSubmit(U('Course/doMerge'))->buttonBack()->data($from)->display();
        }

    }

    public function doMerge($id, $toid)
    {
        $effect_count = D('CourseContent')->where(array('Course_id' => $id))->setField('Course_id', $toid);
        D('Course')->where(array('id' => $id))->setField('status', -1);
        $this->success('合并分类成功。共影响了' . $effect_count . '个内容。', U('Course'));
        //TODO 实现合并功能 Course
    }
    public function post($page = 1, $forum_id = null, $r = 20, $title = '', $content = '')
    {
        //读取帖子数据
        $map = array('status' => array('EGT', 0));
        if ($title != '') {
            $map['title'] = array('like', '%' . $title . '%');
        }
        if ($content != '') {
            $map['content'] = array('like', '%' . $content . '%');
        }
        if ($forum_id) $map['forum_id'] = $forum_id;
        $model = M('ForumPost');
        $list = $model->where($map)->order('last_reply_time desc')->page($page, $r)->select();
        $totalCount = $model->where($map)->count();

        foreach ($list as &$v) {
            if ($v['is_top'] == 1) {
                $v['top'] = L('_STICK_IN_BLOCK_');
            } else if ($v['is_top'] == 2) {
                $v['top'] = L('_STICK_GLOBAL_');
            } else {
                $v['top'] = L('_STICK_NOT_');
            }
        }
        //读取板块基本信息
        if ($forum_id) {
            $forum = M('Forum')->where(array('id' => $forum_id))->find();
            $forumTitle = ' - ' . $forum['title'];
        } else {
            $forumTitle = '';
        }

        $forum1 = M('Forum')->field('id,title')->select();
        array_unshift($forum1, array('id' => '', 'title' => '全部'));
        for ($i = 0; $i < count($forum1); $i++) {
            $forum1[$i]['value'] = $forum1[$i]['title'];
        }
        unset($i);

        //显示后台页面T('Forum@default/Forum/changePlate'
        $builder = new AdminListBuilder();
        $builder->title(L('_POST_MANAGE_') . $forumTitle)
            ->setStatusUrl(U('Forum/setPostStatus'))->buttonEnable()->buttonDisable()->buttonDelete()->buttonModalPopup(U('Forum/changePlate', array('forum_id' => $map['forum_id'])), array(), L('_MIGRATING_NOTE_'), array('data-title' => L('_MIGRATING_NOTE_TO_ANOTHER_PLATE_'), 'target-form' => 'ids'))
            ->selectPlateForm('spf', get, U('Admin/Forum/post'))->select($title = '筛选：', $name = 'forum_id', 'select', '', '', '', $forum1)
            ->keyId()->keyLink('title', L('_TITLE_'), 'Forum/reply?post_id=###')
            ->keyCreateTime()->keyUpdateTime()->keyTime('last_reply_time', L('_LAST_REPLY_TIME_'))->key('top', L('_STICK_YES_OR_NOT_'), 'text')->keyStatus()->keyDoActionEdit('editPost?id=###')
            ->setSearchPostUrl(U('Admin/Forum/post'))->search(L('_TITLE_'), 'title')->search(L('_CONTENT_'), 'content')
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }
    public function student($page = 1, $r = 30, $bj_code="")
    {
        //读取列表
		$aClass=I('bj_code');
        if($aClass){
            $map['bj_code']=$aClass;
        }else{
            unset($map['bj_code']);
        }
        $map['status']=1;
        //dump($map);die;
        // $map = array('status' => 1, 'bj_code'=>$class);
        $model = M('Member');
        $list = $model->where($map)->order('st_code asc')->page($page, $r)->select();
        // dump($list);die;
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';

        /*获取班级列表信息*/
        $bj_list = D('JfClass')->field('bj_code as id,bj_name ')->select();
        array_unshift($bj_list, array('id' => '', 'bj_name' => '全部'));
        for ($i = 0; $i < count($bj_list); $i++) {
            $bj_list[$i]['value'] = $bj_list[$i]['bj_name'];
        }
        unset($i);        
        // dump($bj_list);die;
        $builder->title('学生管理')
            ->selectPlateForm('id', get, U('Admin/Course/student'))
            // select($title = '筛选', $name = 'key', $type = 'select', $des = '', $attr, $arrdb = '', $arrvalue = null)
            ->select($title = '班级：', $name = 'bj_code', 'select', 'id', 'bj_name', '', $bj_list)
            //->setStatusUrl(U('setCourseContentStatus'))->buttonDisable('', '审核不通过')->buttonDelete()
            ->keyId('uid','用户ID')->keyText('st_code','学号')
            //->keyLink('title', '标题', 'Course/student/CourseContentDetail?id=###')
            ->keyText('nickname','用户名')->keyText('pc','登录主机')
            ->keyCreateTime('reg_time','注册时间')->keyUpdateTime('last_login_time','最近登录')->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
           
    }
        public function bjstudent($page = 1, $r = 30)
    {
        //读取列表
        $class=I('bjcode')?I('bjcode'):201001;
        $map = array('status' => 1, 'bj_code'=>$class);
        $model = M('JfStudent');
        $list = $model->where($map)->page($page, $r)->select();
        unset($li);
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';


        $builder->title('学生管理')
            ->setStatusUrl(U('setCourseContentStatus'))->buttonDisable('', '审核不通过')->buttonDelete()
            ->keyId()->keyStName()->keyLink('title', '标题', 'Course/student/CourseContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }
	public function stdoc($page = 1, $r = 20)
    {
        //读取列表
        $map = array('status' => 1);
        $model = M('UcenterMember');
        $list = $model->where($map)->page($page, $r)->select();
        unset($li);
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';


        $builder->title('视频管理')
            ->setStatusUrl(U('setCourseContentStatus'))->buttonDisable('', '审核不通过')->buttonDelete()
            ->keyId()->keyLink('title', '标题', 'Course/student/CourseContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }
   

    public function verify($page = 1, $r = 10)
    {
        //读取列表
        $map = array('status' => 0);
        $model = M('CourseContent');
        $list = $model->where($map)->page($page, $r)->select();
        unset($li);
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';


        $builder->title('审核内容')
            ->setStatusUrl(U('setCourseContentStatus'))->buttonEnable('', '审核通过')->buttonDelete()
            ->keyId()->keyLink('title', '标题', 'Course/Index/CourseContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }

    public function setCourseContentStatus()
    {
        $ids = I('ids');
        $status = I('get.status', 0, 'intval');
        $builder = new AdminListBuilder();
        if ($status == 1) {
            foreach ($ids as $id) {
                $content = D('CourseContent')->find($id);
                D('Common/Message')->sendMessage($content['uid'],$title = '视频内容审核通知', "管理员审核通过了您发布的内容。现在可以在列表看到该内容了。",  'Course/Index/CourseContentDetail', array('id' => $id), is_login(), 2);
                /*同步微博*/
                /*  $user = query_user(array('nickname', 'space_link'), $content['uid']);
                  $weibo_content = '管理员审核通过了@' . $user['nickname'] . ' 的内容：【' . $content['title'] . '】，快去看看吧：' ."http://$_SERVER[HTTP_HOST]" .U('Course/Index/CourseContentDetail',array('id'=>$content['id']));
                  $model = D('Weibo/Weibo');
                  $model->addWeibo(is_login(), $weibo_content);*/
                /*同步微博end*/
            }

        }
        $builder->doSetStatus('CourseContent', $ids, $status);

    }

    public function contentTrash($page = 1, $r = 10, $model = '')
    {
        //读取微博列表
        $builder = new AdminListBuilder();
        $builder->clearTrash($model);
        $map = array('status' => -1);
        $model = D('CourseContent');
        $list = $model->where($map)->page($page, $r)->select();
        $totalCount = $model->where($map)->count();

        //显示页面

        $builder->title('内容回收站')
            ->setStatusUrl(U('setCourseContentStatus'))->buttonRestore()->buttonClear('CourseContent')
            ->keyId()->keyLink('title', '标题', 'Course/Index/CourseContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }
}
