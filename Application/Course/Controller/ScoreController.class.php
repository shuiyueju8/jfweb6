<?php
namespace Course\Controller;
use Think\Controller;


/**
 * 前台积分控制器
 * 主要获取首页聚合数据
 */
class ScoreController extends Controller
{
    protected function _initialize()
    {   
        // $adminid=is_administrator();
        if(!is_administrator()){
            $this->error('非管理员，不能操作此模块',U('/Home/Index'));
        }
        
        if(is_login()){
	        $cur_bj=cookie('cur_bj');
	        if(!$cur_bj) {
	        	$cur_bj=D('JFClass')->where('cur = 1')->find();
		        cookie('cur_bj',$cur_bj,360000);//设置传递默认班级
	           
	          }
        }
        $this->classModel=D('Course/JfClass');
        $classlist=$this->classModel->field('status',true)->order('bj_code','asc')->where($map)->select();
        $this->assign('classlist',$classlist);
        $this->xinxiModel=D('Course/JfXinxijishu');
        //设置导航条信息

         $bj_code=I('bj_code',$cur_bj['bj_code']);

         $map=array('bj_code'=>$bj_code);
         $aClass=$this->classModel->where($map)->find();
         $sub_menu['left']= array(
            array(
                'tab' => 'home', 'title' => $aClass['bj_name'],
                'href' =>  U('ClassCourse/setcurclass','bj_code='.$bj_code)
                ),
           array(
            'tab'=>'kecheng','title'=>$aClass['title'],
            'href'=>U('ClassCourse/selectBjCourse','bj_code='.$bj_code)
            )
           );
        $this->assign('sub_menu', $sub_menu);
        $this->assign('cur', $cur_bj);
        $this->assign('bj_code',$bj_code);
        $this->assign('now_tab',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
       
    }
    //教师管理首页
    public function index()
    {

     $bj_code=I('bj_code',$cur_bj['bj_code']);
     $classmodel=$this->classModel;
     $map=array('bj_code'=>$bj_code);
     $aClass=$classmodel->where($map)->find();  
           
        $bjlist=$classmodel->order('bj_code','asc')->select();
        
        $this->assign('curclass', $aClass);
      
        $this->assign('current', 'Course');        
        $this->display();

    }

    //积分管理首页
    public function score()
    {
        $bj_code=I('bj_code')?I('bj_code'):(cookie('bj_code')?cookie('bj_code'):session('user_auth.bj_code'));         
        if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
            cookie("bj_code", $bj_code, 36000);
        }
        $map['status'] = array('gt', 0);
        $map['bj_code']= $bj_code;
		$map['uid']=array('not in',str2arr(C('USER_ADMINISTRATOR')));
        // dump($map);//die;
        $uid =D('Member')->where($map)->field('uid')->order(array('pc'=>'asc'))->select();
         //dump($uid);
        foreach($uid as $key=>$v){
        	// dump($key);dump($v);
        	$list[$key]=D('member')->field('uid','st_code','sex','nickname','pc','username','score1','score2','score3','score4','score5','zuopin_count')
        	->find((int)$v['uid']);
            
        }
         // dump($list);die;//list_sort_by($list, $field, $sortby = 'asc')
		//$list=list_sort_by($list,score1,desc);
		$list=list_sort_by($list,st_code,asc);
        //exit;
        $this->thistitle= '学生积分管理' ;
        $this->assign('cur',$bj_code);       
        $this->assign('list',$list);
        $this->display();
    }
        //积分管理首页
    public function score_asc()
    {
        $bj_code=I('bj_code')?I('bj_code'):(cookie('bj_code')?cookie('bj_code'):session('user_auth.bj_code'));         
        if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
            cookie("bj_code", $bj_code, 36000);
        }
        $map['status'] = array('gt', 0);
        $map['bj_code']= $bj_code;
		$map['uid']=array('not in',str2arr(C('USER_ADMINISTRATOR')));

        $uid =D('Member')->where($map)->field('uid')->order(array('pc'=>'asc'))->select();

        foreach($uid as $key=>$v){

            $list[$key]=query_user(array('uid','st_code','sex','nickname','pc','username','score1','score2','score3','score4',),(int)$v['uid']);
 
        }

		$list=list_sort_by($list,score1,desc);
        //exit;
        $this->thistitle= '学生积分排行榜' ;
        $this->assign('cur',$bj_code);       
        $this->assign('list',$list);
        $this->display();
    }

    //学生扣分管理
    public function scoredec()
    {
        $cur_bj=cookie('cur_bj');
        $bj_code=I('bj_code')?I('bj_code'):$cur_bj['bj_code']; 

        
        if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
            cookie("bj_code", $bj_code, 36000);
        }
        $perfModel=D('JfPerformance');
        $this->scorelist=$perfModel->where(array('type'=>'Dec'))->select();
        //dump($list);
        $map['status'] = array('egt', 0);
        $map['bj_code']= $bj_code;
        $map['uid']=array('not in',str2arr(C('USER_ADMINISTRATOR')));
        $list =D('Member')->where($map)
        ->field('uid,nickname,pc,st_code,last_login_time, last_login_ip,score4,score5')
        ->order(array('pc'=>'asc'))
        ->select();
        // dump($list);die;
        if(IS_POST){
            $uids=I('post.ids');

            $decid=I('post.dec_id');
            $dec=$perfModel->find($decid);
            $scoreModel= D('Ucenter/Score') ; 
             // dump($uids);die;      
                //积分减10 
                 $score=$dec['score'];
                 $tip='积分'.$score;
                 $remark=$dec['remark'].$tip;//记录参数
                  D('Common/Message')->sendMessage($uids, $title = '您被扣分了', $remark,  'Score/index/', $score, is_login(), 2);
                $scoreModel->setUserScore($uids, $score, $type='4', $action = 'dec', $action_model ='Per', $record_id,$remark);
                $scoreModel->setUserScore($uids, $score, $type='1', $action = 'dec', $action_model ='Per', $record_id,$remark);
             
                 if($rs=1){
                    $this->success('扣分成功，扣分原因是:'. $dec['title'].','.$tip);                    
                 }else{
                    $this->success('保存失败！');
                 }               
        } 
        $this->thistitle= '学生扣分登记' ; 
        $this->assign('cur',$bj_code);  
        $this->assign('list',$list);
        $this->display();
    }
    
    //学生加分管理
    public function scoreinc()
    {
        $bj_code=I('bj_code')?I('bj_code'):(cookie('bj_code')?cookie('bj_code'):session('user_auth.bj_code')); 
        
        if(cookie('bj_code')!=$bj_code){ //如果班级不一致设置cookie
            cookie("bj_code", $bj_code, 36000);
        }
        $perfModel=D('JfPerformance');
        $this->scorelist=$perfModel->where(array('type'=>'Inc'))->select();
        //dump($list);
        $map['status'] = array('egt', 0);
        $map['bj_code']= $bj_code;
        //$map['id']=array('not in',str2arr(C('USER_ADMINISTRATOR')));
        $order=array('pc'=>'asc');
        $list =D('Member')->where($map)->order($order)
        ->field('uid,st_name,nickname,pc,st_code,last_login_time, last_login_ip,score5')->select();


        if(IS_POST){
            $uids=I('post.ids');
            $incid=I('post.inc_id');
            $inc=$perfModel->find($incid);
            $scoreModel= D('Ucenter/Score') ; 
     
                //积分减10 
                 $score=$inc['score'];
                 $tip='积分+'.$score;

                 $remark=$inc['remark'].$tip;//记录参数
                  D('Common/Message')->sendMessage($uids, $title = '您被加分了', $remark,  'Score/index/', $score, is_login(), 2);
                $scoreModel->setUserScore($uids, $score, $type='5', $action = 'inc', $action_model ='Per', $record_id=$u,$remark);
                $scoreModel->setUserScore($uids, $score, $type='1', $action = 'inc', $action_model ='Per', $record_id=$u,$remark);             
                 if($rs=1){
                    $this->success('加分成功，加分原因是:'. $inc['title'].','.$tip);                    
                 }else{
                    $this->success('保存失败！');
                 }
               
        } 
        $this->thistitle= '学生加分登记' ; 
        $this->assign('cur',$bj_code);  
        $this->assign('list',$list);
        $this->display();
    }


}