<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

namespace Course\Model;
use Think\Model;

/**
 * Class CourseModel 课程班级模型
 * @package Course\Model
 * @auth 陈一枭
 */
class JfXinxijishuModel extends Model {

    protected $tableName='jf_xinxijishu';
    protected $_validate = array(
        array('title', '1,100', '标题长度不符', self::EXISTS_VALIDATE, 'length'),
        array('grade','require','年级一般为3A、3B'), //默认情况下用
        array('grade_t','require','年级一般为三上、三下'), //默认情况下用
    );
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_UPDATE),
        array('status', '1', self::MODEL_INSERT),
    );


}