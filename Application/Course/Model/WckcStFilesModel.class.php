<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------
namespace Course\Model;
use Think\Model;
use Think\Page;

/**
 * Class ZuopinModel 作品展示模型
 * @package Zuopin\Model
 * @auth 水月居
 */
class WckcStFilesModel extends Model {

    //protected $trueTableName='wgy_jf_stdoc';
    protected $_validate = array(
	    array('title','require','title必须填写'),
       //array('url','require','url必须填写'), //默认情况下用正则进行验证
     );
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );



}