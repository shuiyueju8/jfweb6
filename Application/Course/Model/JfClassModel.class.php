<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

namespace Course\Model;
use Think\Model;

/**
 * Class CourseModel 课程班级模型
 * @package Course\Model
 * @auth 水月居
 */
class JfClassModel extends Model {

    protected $tableName='jf_class';
    protected $_validate = array(
        array('bj_code','require','班级代码必须填写，通常设置为学生入学年份+班级序号'), //默认情况下用正则进行验证
    );
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
    );

}