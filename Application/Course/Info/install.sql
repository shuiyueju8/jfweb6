-- -----------------------------
-- 表结构 `ocenter_course_content`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_course_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `sort` tinyint(8) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_course_grade`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_course_grade` (
  `id` tinyint(6) unsigned NOT NULL AUTO_INCREMENT,
  `grade` char(2) NOT NULL,
  `grade_t` char(10) NOT NULL,
  `sort` int(10) NOT NULL,
  `text` char(30) DEFAULT NULL COMMENT '备注',
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_course_type`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_course_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_course_xinxijishu`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_course_xinxijishu` (
  `id` int(3) NOT NULL DEFAULT '0' COMMENT 'id',
  `grade` varchar(2) DEFAULT NULL COMMENT '册数id',
  `grade_t` varchar(6) DEFAULT NULL COMMENT '册数',
  `title` varchar(39) DEFAULT NULL COMMENT '课程标题',
  `up_cat` varchar(3) DEFAULT NULL COMMENT '上传类型',
  `allow_ext` varchar(50) DEFAULT NULL COMMENT '允许上传扩展名',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

-- -----------------------------
-- 表内记录 `ocenter_course_content`
-- -----------------------------
INSERT INTO `ocenter_course_content` VALUES ('1', '92', '第7课认识动画', '<p><a href=\"5B-7.ppt\"></a></p><p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://127.0.0.1/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_ppt.gif\"/><a target=\"_blank\" style=\"color: rgb(0, 102, 204); font-size: 18px; text-decoration: underline;\" href=\"/jfweb2016/Uploads/Editor/File/2016-04-12/570d039405824.ppt\" title=\"5B-7.ppt\"><strong><span style=\"font-size: 18px;\">课件下载</span></strong></a> <br/> <a href=\"rw02.rar\"></a><br/></p><h2><a style=\"font-size: 16px; text-decoration: underline;\" href=\"gif1.html\"><span style=\"font-size: 16px;\">动画素材</span></a></h2><p><img alt=\"gif05.GIF\" title=\"undefined\" src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cf8abec301.GIF\"/> &nbsp; &nbsp;<img src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cf8efb5228.gif\" style=\"\"/> &nbsp; &nbsp;<img src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cf8efc13a6.gif\" style=\"\"/> &nbsp; &nbsp;<img src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cf8efcbe9f.gif\" style=\"\"/></p>', '1', '1460549607');
INSERT INTO `ocenter_course_content` VALUES ('2', '92', '任务一', '<p><span style=\"font-size: 18px;\">1、访问学校的学科平台，成功下载到了GIF动画作品。 <br/></span></p><p><span style=\"font-size: 18px;\">2、将动画作品插入到ppt中，并将文件保存到任务一文件夹中，命名为“xxx的动画.pptx”。</span></p><p><span style=\"font-size: 18px;\"></span></p><p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://127.0.0.1/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_rar.gif\"/><a target=\"_blank\" style=\"color: rgb(0, 102, 204); font-size: 18px; text-decoration: underline;\" href=\"/jfweb2016/Uploads/Editor/File/2016-04-12/570d069cca941.rar\" title=\"任务一.rar\"><strong><span style=\"font-size: 18px;\">任务一.rar</span></strong></a></p><p><span style=\"font-size: 18px;\"></span></p><p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://www.ttwgy.com/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_ppt.gif\"/><a target=\"_blank\" style=\"color: rgb(0, 102, 204); font-size: 20px; text-decoration: underline;\" href=\"/jfweb2016/Uploads/Editor/File/2016-04-13/570db59f9ba41.pptx\" title=\"我型我秀.pptx\"><span style=\"font-size: 20px;\">我型我秀.pptx</span></a></p><p><br/></p>', '2', '1460516275');
INSERT INTO `ocenter_course_content` VALUES ('3', '92', '任务二', '<p><span style=\"font-size: 18px;\">1.用“Ulead GIF Animator”打开动画，想一想动画图片为什么看起来会动？<br/>2.查看下面的动画图片&quot;小狗.gif&quot;、&quot;小猫.gif&quot;图片中分别有几帧，几个对象？</span><br/></p><p><img src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cfa8868a89.gif\" style=\"\"/> <img src=\"/jfweb2016/Uploads/Editor/Picture/2016-04-12/570cfa887c71a.GIF\" style=\"\"/></p><p><span style=\"text-decoration: underline;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>帧，<span style=\"text-decoration: underline;\"> &nbsp; &nbsp;&nbsp; &nbsp; </span>对象 &nbsp; &nbsp; <span style=\"text-decoration: underline;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span>帧，<span style=\"text-decoration: underline;\"> &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </span>对象</p>', '4', '1460480345');
INSERT INTO `ocenter_course_content` VALUES ('4', '92', '知识链接', '<p><span style=\"font-size: 20px;\"><strong>动画为什么看起来会动</strong></span><br/></p><p><br/></p><p><span style=\"font-family: 宋体,SimSun; font-size: 18px;\">&nbsp;&nbsp; 动画顾名思义就是活动的画，由连续的画面组成在人的视觉中产生画面上的人或物在动的印象，就是动画。<br/>&nbsp;&nbsp; 动画的原理是人眼的<span style=\"font-size: 18px; color: rgb(255, 0, 0); font-family: 微软雅黑,Microsoft YaHei;\">视觉暂留</span>现象，也就是说，一组画面连续快速的在人眼前出现，在前一幅画还未从人的视网膜上消失时，就接上下一幅画面，这两幅画面就“接”在了一起，使人们产生画面在动的错觉，也就产生了动画。</span><span style=\"font-family: 宋体,SimSun;\"></span><br/><br/></p>', '3', '1460480764');
INSERT INTO `ocenter_course_content` VALUES ('5', '92', '任务三', '<p><span style=\"font-size: 18px;\">课后小练习：收集更多的动画素材与相关资料，与同学分享你的学习成果。</span><br/></p>', '5', '1460593506');
INSERT INTO `ocenter_course_content` VALUES ('6', '70', '邮箱的格式', '<p>邮箱的格式：</p><p>ttwgypxl@163.com</p><ol style=\"list-style-type: decimal;\" class=\" list-paddingleft-2\"><li><p>@是邮箱的标识符 at (在)<br/></p></li><li><p>前面是用户名</p></li><li><p>后面是服务器域名<br/></p></li></ol>', '1', '1536892549');
INSERT INTO `ocenter_course_content` VALUES ('7', '70', '一、给老师发一封电子邮件', '<p>1、给老师发一封电子邮件，老师的邮箱是ttwgypxl@163.com<br/></p>', '2', '1536892588');
INSERT INTO `ocenter_course_content` VALUES ('9', '84', '一、搜寻计算组成信息', '<p>从英特网搜寻计算组成信息，制作成word文档。</p><p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://127.0.0.1/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_doc.gif\"/><a target=\"_blank\" style=\"font-size:12px; color:#0066cc;\" href=\"/jfweb2016/Uploads/Editor/File/2017-09-24/59c7a6699279b.doc\" title=\"时间管理选择题.doc\">时间管理选择题.doc</a></p><p><br/></p>', '1', '1506256492');
INSERT INTO `ocenter_course_content` VALUES ('10', '152', '任务一 做一个学校风光的数字相册', '<p>&nbsp;将提供的学校照片利用Powerpoint做一个数字相册，并附上音乐。<br/></p>', '1', '1506304672');
INSERT INTO `ocenter_course_content` VALUES ('11', '70', '三、收集同学的邮箱', '<p>收集同学的邮箱地址，并给你的好朋友发一封问候的邮件。</p><p>常见的邮箱服务器地址：</p><p>1.163邮箱 <a href=\"http://mail.163.com\" target=\"_blank\">http://mail.163.com</a><br/></p><p>2.QQ邮箱 <a href=\"http://mail.qq.com\" target=\"_self\">http://mail.QQ.com</a><br/></p>', '3', '1536893122');
INSERT INTO `ocenter_course_content` VALUES ('12', '36', '作业（设置文本格式）', '<p>将“钓鱼.docx&quot; 文档设置合适的字体、字号、颜色。</p><p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://10.110.103.88/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_doc.gif\"/><a target=\"_blank\" style=\"font-size:12px; color:#0066cc;\" href=\"/jfweb2016/Uploads/Editor/File/2018-09-14/5b9b6dcc8c870.docx\" title=\"钓鱼.docx\">钓鱼.docx</a></p><p><br/></p>', '1', '1536912847');
INSERT INTO `ocenter_course_content` VALUES ('13', '37', '钓鱼排版', '<p style=\"line-height: 16px;\"><img style=\"vertical-align: middle; margin-right: 2px;\" src=\"http://10.110.103.88/jfweb2016/Public/static/ueditor/dialogs/attachment/fileTypeImages/icon_doc.gif\"/><a target=\"_blank\" style=\"font-size:12px; color:#0066cc;\" href=\"/jfweb2016/Uploads/Editor/File/2018-09-19/5ba1b1d336cfa.docx\" title=\"钓鱼1.docx\">钓鱼1.docx</a></p><p>将上面的文档设置页面格式后上交。<br/></p>', '1', '1537323499');
INSERT INTO `ocenter_course_content` VALUES ('14', '71', '1、发送带有附件的邮件', '<p>任务一：<br/></p><p>发送一封带有附件的邮件至ttwgypxl@163.com</p>', '1', '1537925065');
INSERT INTO `ocenter_course_content` VALUES ('15', '71', '2.邮件的定时发送功能 ', '<p>试着给你的好朋友定时发送一封生日祝福邮件，并附上一首祝福的歌曲。</p>', '2', '1537925157');
INSERT INTO `ocenter_course_content` VALUES ('16', '38', '任务一 制作自己的作文集封面', '<p>制作自己的作文集封面，并以班级-学号-作文集名称“作业文件名保存到桌面上。</p>', '1', '1537929002');
-- -----------------------------
-- 表内记录 `ocenter_course_grade`
-- -----------------------------
INSERT INTO `ocenter_course_grade` VALUES ('1', '6A', '六上', '1', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('2', '6B', '六下', '2', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('3', '5A', '五上', '3', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('4', '5B', '五下', '4', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('5', '4A', '四上', '5', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('6', '4B', '四下', '6', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('7', '3A', '三上', '7', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('8', '3B', '三下', '8', '信息技术', '1');
INSERT INTO `ocenter_course_grade` VALUES ('9', '7A', '上册', '9', '电脑制作班', '1');
INSERT INTO `ocenter_course_grade` VALUES ('10', '7B', '下册', '10', '电脑制作班', '1');
-- -----------------------------
-- 表内记录 `ocenter_course_type`
-- -----------------------------
INSERT INTO `ocenter_course_type` VALUES ('1', '素材', '122314324');
INSERT INTO `ocenter_course_type` VALUES ('2', '课堂任务', '0');
INSERT INTO `ocenter_course_type` VALUES ('3', '课件', '0');
-- -----------------------------
-- 表内记录 `ocenter_course_xinxijishu`
-- -----------------------------
INSERT INTO `ocenter_course_xinxijishu` VALUES ('1', '3A', '三上', '第1课 与新朋友见面', 'doc', 'rtf', '1458731535', '1458731535', '1', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('2', '3A', '三上', '第2课 可爱的鼠标', 'pic', 'jpg,bmp', '1458731535', '1458731535', '2', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('3', '3A', '三上', '第3课 鼠标陪我玩一玩', 'pic', 'jpg', '1458731535', '1458731535', '3', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('4', '3A', '三上', '第4课 计算器帮我忙', 'doc', 'rtf,doc', '1458731535', '1458731535', '4', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('5', '3A', '三上', '第5课 小伙伴的作品', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '5', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('6', '3A', '三上', '第6课 小树苗快快长', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '6', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('7', '3A', '三上', '第7课 整齐的教学楼', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '7', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('8', '3A', '三上', '第8课 五彩的窗子', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('9', '3A', '三上', '第9课 花香满园', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('10', '3A', '三上', '第10课 我能画得更好', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('11', '3A', '三上', '第11课 美丽的校园', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('12', '3A', '三上', '第12课 有趣的画板', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('13', '3A', '三上', '第13课 丰富的图库', 'pic', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('14', '3A', '三上', '第14课 神秘的暗房', 'doc', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('15', '3A', '三上', '第15课 贺新年', 'doc', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('16', '3A', '三上', '三上综合练习', 'doc', 'jpg,bmp,rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('17', '3A', '三上', '三上考试测评', 'doc', 'bmp,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('18', '3B', '三下', '第1课 指挥窗口', 'doc', 'jpg,bmp,rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('19', '3B', '三下', '第2课 装扮桌面', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('20', '3B', '三下', '第3课 键盘一家', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('21', '3B', '三下', '第4课 接触上排键', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('22', '3B', '三下', '第5课 接触下排键', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('23', '3B', '三下', '第6课 键盘指法练习', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('24', '3B', '三下', '第7课 感受双符号键', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('25', '3B', '三下', '第8课 大小写巧输入', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('26', '3B', '三下', '第9课 多样的输入法', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('27', '3B', '三下', '第10课 古诗一首', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('28', '3B', '三下', '第11课 词语输入速度快', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('29', '3B', '三下', '第12课 连词成句变化多', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('30', '3B', '三下', '第13课 串句成文巧修饰', 'doc', 'rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('31', '3B', '三下', '第14课 编辑科普短文', 'doc', 'doc,rtf', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('32', '3B', '三下', '第15课 制作宣传海报', 'doc', 'doc,jpg,bmp', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('33', '3B', '三下', '三下综合练习', 'doc', 'jpg,rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('34', '3B', '三下', '三下考试测评', 'doc', 'jpg,rtf,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('35', '4A', '四上', '第1课 初识文字处理软件', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('36', '4A', '四上', '第2课 设置文本格式', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('37', '4A', '四上', '第3课 调整页面布局', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('38', '4A', '四上', '第4课 设计文集封面', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('39', '4A', '四上', '第5课 汇集作文集', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('40', '4A', '四上', '第6课 初试学习平台', 'doc', 'doc,jpg', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('41', '4A', '四上', '第7课 参与网上学习', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('42', '4A', '四上', '第8课 作品上传与分享', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('43', '4A', '四上', '第9课 制作通讯录', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('44', '4A', '四上', '第10课 斟词酌句理文本', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('45', '4A', '四上', '第11课 画龙点睛写标题', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('46', '4A', '四上', '第12课 图文并茂美文章', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('47', '4A', '四上', '第13课 有的放矢查资料', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('48', '4A', '四上', '第14课 灵活运用文本框', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('49', '4A', '四上', '第15课 精益求精做小报', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('50', '4A', '四上', '四上综合练习', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('51', '4A', '四上', '四上考试测评', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('52', '4B', '四下', '第1课 生活与网络', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('53', '4B', '四下', '第2课 浏览器的使用', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('54', '4B', '四下', '第3课 分类查找', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('55', '4B', '四下', '第4课 搜索引擎', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('56', '4B', '四下', '第5课 网络摘记', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('57', '4B', '四下', '第6课 网络音乐厅', 'doc', 'doc,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('58', '4B', '四下', '第7课 整理音乐库', 'doc', 'doc,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('59', '4B', '四下', '第8课 虚拟旅游', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('60', '4B', '四下', '第9课 分享图片', 'doc', 'doc,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('61', '4B', '四下', '第10课 节日信息搜集', 'doc', 'doc,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('62', '4B', '四下', '第11课 节日资料整理', 'doc', 'doc,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('63', '4B', '四下', '第12课 节日文档制作', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('64', '4B', '四下', '第13课 标志知识收集', 'doc', 'doc,docx,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('65', '4B', '四下', '第14课 标志知识介绍', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('66', '4B', '四下', '第15课 设计制作标志', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('67', '4B', '四下', '四下综合练习', 'doc', 'doc,jpg,bmp,sb,', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('68', '4B', '四下', '四下考试测评', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('69', '5A', '五上', '第1课 相聚在网上', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('70', '5A', '五上', '第2课 网上谈旅游', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('71', '5A', '五上', '第3课 读书论坛', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('72', '5A', '五上', '第4课 采集生活点滴', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('73', '5A', '五上', '第5课 回顾多彩生活', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('74', '5A', '五上', '第6课 精彩照片剪辑', 'pic', 'jpg,doc,bmp', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('75', '5A', '五上', '第7课 轻松调节图片', 'doc', 'jpg,bmp,png', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('76', '5A', '五上', '第8课 聆听美妙乐曲', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('77', '5A', '五上', '第9课 乐曲变化多样', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('78', '5A', '五上', '第10课 创作MIDI乐曲', 'doc', 'mid', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('79', '5A', '五上', '第11课 制作电子相册', 'doc', 'scr,exe', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('80', '5A', '五上', '第12课 电子相册礼包', 'doc', 'scr,exe', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('81', '5A', '五上', '第13课 收集资料方法多', 'doc', 'doc,rar', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('82', '5A', '五上', '第14课 网络讨论会', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('83', '5A', '五上', '第15课 编辑研究报告', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('84', '5A', '五上', '五上综合练习', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('85', '5A', '五上', '五上考试测评', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('86', '5B', '五下', '第1课 生活在信息中', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('87', '5B', '五下', '第2课 现代信息技术', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('88', '5B', '五下', '第3课 用计算机处理信息', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('89', '5B', '五下', '第4课 初识Powerpoint', 'doc', 'doc,ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('90', '5B', '五下', '第5课 丰富幻灯片内容', 'doc', 'doc,ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('91', '5B', '五下', '第6课 让幻灯片动起来', 'doc', 'doc,ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('92', '5B', '五下', '第7课 认识动画', 'doc', 'gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('93', '5B', '五下', '第8课 修改动画', 'doc', 'gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('94', '5B', '五下', '第9课 文字动画', 'doc', 'gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('95', '5B', '五下', '第10课 人物动画', 'doc', 'gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('96', '5B', '五下', '第11课 准备演示文稿资料', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('97', '5B', '五下', '第12课 制作演示文稿', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('98', '5B', '五下', '第13课 让演示文稿有声有色', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('99', '5B', '五下', '第14课 灵活的超链接', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('100', '5B', '五下', '第15课 演示播放技巧多', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('101', '5B', '五下', '五下综合练习', 'doc', 'doc,ppt', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('102', '5B', '五下', '五下考试测评', 'doc', 'doc,ppt', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('103', '6A', '六上', '第1课 走进计算机', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('104', '6A', '六上', '第2课 未来的计算机', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('105', '6A', '六上', '第3课 小小工程师', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('106', '6A', '六上', '第4课 安全小博士', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('107', '6A', '六上', '第5课 软硬兼施', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('108', '6A', '六上', '第6课 学习好导师', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('109', '6A', '六上', '第7课 网上图书馆', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('110', '6A', '六上', '第8课 电子小助手', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('111', '6A', '六上', '第9课 旅游计划书', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('112', '6A', '六上', '第10课 确定调查主题与方案', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('113', '6A', '六上', '第11课 设计调查问卷', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('114', '6A', '六上', '第12课 数据的收集与整理', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('115', '6A', '六上', '第13课 数据处理与分析', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('116', '6A', '六上', '第14课 撰写调查报告', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('117', '6A', '六上', '第15课 展示调查成果', 'doc', 'ppt,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('118', '6A', '六上', '六上综合练习', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('119', '6A', '六上', '六上考试测评', 'doc', 'ppt,doc,pptx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('120', '6B', '六下', '第1课 汉字的发展', 'doc', 'ppt,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('121', '6B', '六下', '第2课 设计房间', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('122', '6B', '六下', '第3课 日历桌面', 'doc', 'doc,jpg,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('123', '6B', '六下', '第4课 撰写小论文', 'doc', 'doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('124', '6B', '六下', '第5课 编制毕业留念册', 'doc', 'doc,docx,pptx,p', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('125', '6B', '六下', '第6课 初识Scratch', 'doc', 'sb,doc,docx', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('126', '6B', '六下', '第7课 角色对话', 'doc', 'doc,sb', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('127', '6B', '六下', '第8课 载歌载舞', 'doc', 'sb,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('128', '6B', '六下', '第9课 键盘触发', 'doc', 'sb,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('129', '6B', '六下', '第10课 创编游戏', 'doc', 'sb,doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('130', '6B', '六下', '第11课 走进机器人', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('131', '6B', '六下', '第12课 机器人邮递员', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('132', '6B', '六下', '第13课 机器人导游', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('133', '6B', '六下', '第14课 机器人卫士', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('134', '6B', '六下', '第15课 机器人避障', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('135', '6B', '六下', '六下综合练习', 'doc', 'doc,sb,ppt,jpg,', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('136', '6B', '六下', '六下考试测评', 'doc', 'doc', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('137', '6B', '六下', '万花筒', 'doc', 'sb,exe,gif', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('138', '5B', '五下', '万花筒', 'doc', 'sb,exe,bmp,jpg', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('139', '6B', '六下', '跳舞的女孩', 'doc', 'sb', '1458731535', '1458731535', '0', '1');
INSERT INTO `ocenter_course_xinxijishu` VALUES ('140', '7A', '动画上', '第1课 初始认识flash', 'doc', 'swf,fla', '', '', '1', '');
