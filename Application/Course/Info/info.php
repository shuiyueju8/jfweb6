<?php

return array(
    //模块名
    'name' => 'Course',
    //别名
    'alias' => '课程',
    //版本号
    'version' => '6.0.1',
    //是否商业模块,1是，0，否
    'is_com' => 0,
    //是否显示在导航栏内？  1是，0否
    'show_nav' => 1,
    //模块描述
    'summary' => '课程模块，适用于学生课程学习的记录与展示',
    //开发者
    'developer' => '水月居科技有限公司',
    //开发者网站
    'website' => 'http://blog.sina.com.cn/shuiyueju8',
    //前台入口，可用U函数
    'entry' => 'Course/index/index',

    'admin_entry' => 'Admin/course/student',

    'icon' => 'book',

    'can_uninstall' =>1
);