<?php
// +----------------------------------------------------------------------
// | shuiyueju [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.zjttwgy.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: singliang <singliang@163.com> <http://blog.sina.com.cn/shuiyueju8>
// +----------------------------------------------------------------------

// 用户自定义函数

// 用户自定义函数


/*手机浏览时跳转*/
function redirect_m($url)
{
    if(is_m()) {
        $url = str_replace(__ROOT__, '', $url);
        $url = __ROOT__ . '/m' . $url;
        redirect($url);
    }
}


 //检查用户登录计算机是否指定位置
    function check_pc($uid){ 
    if(C('LOGIN_PC_LIMIT')){

       if(!is_administrator($uid)){
    
        $check=D('Member')->field('pc,uid')->find($uid);

       if(!$check['pc']){
           $check['pc']=get_client_pc();
           // UCenterMember()->save($check);
           D('Member')->save($check);
           return true;
          }
      $allowpc=explode(',', $check['pc']);
	    $loginpc=get_client_pc();

	    if(in_array($loginpc,$allowpc)){
	        return true;
	    }else{
	        return false;
	    }
    }
		
	}
	return true;
  }

  
//获取学生机器名
function get_client_pc()
    {
        $ip=get_client_ip();
        $pc='T'.substr($ip,-2);
       return $pc; //返回学生机器名
    }

//头像上传文件命名规则
function avatar_saverule(){
    $aid=is_login();
    $afilename='/'.$aid.'/U'.$aid;
    return $afilename;
} 

/**去除字符串中所有空格
*/
function trimall($str)//删除空格
{
    $qian=array(" ","　","\t","\n","\r");$hou=array("","","","","");
    return str_replace($qian,$hou,$str);    
}

function get_bjname($bj_code = 0)
{
    static $class_list;

    /* 获取缓存数据 */
    if (empty($class_list)) {
        $class_list = S('sys_class_list');
    }

    /* 查找班级名称 */
    $key = "b{$bj_code}";
    if (isset($class_list[$key])) { //已缓存，直接使用
        $bj_name = $class_list[$key];
    } else { //调用接口获取用户信息
        $info = M('JfClass')->field('bj_name')->where(array('bj_code'=>$bj_code))->find();
        if ($info !== false && $info['bj_name']) {
            $bj_name = $info['bj_name'];
            $class_list[$key] = $bj_name;
            /* 缓存班级 */
            $count = count($class_list);
            $max = 100;// C('USER_MAX_CACHE')
            while ($count-- > $max) {
                array_shift($class_list);//删除除第一个键值的数组
            }
            S('sys_class_list', $class_list);
        } else {
            $bj_name = '';
        }
    }
    return $bj_name;
}
  //获取作品文件小图标
 function get_zuopin_ico($file_ext){
   
        $img_path= './Application/zuopin/Static/images/filetype32/'.$file_ext.'.gif';
   
        if(file_exists($img_path)){
       $file_path=$img_path;        
        }else{        
          $file_path='./Application/zuopin/Static/images/filetype32/unknown.gif';
        } 
    return $file_path;
 }
 //获取性别
 function get_sex($sex){
  if($sex==1){
    return "男";
    }elseif($sex==2){
    return "女";  
    }else{
    return "未知";  
    }
 }