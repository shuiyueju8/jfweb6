<?php

/**
 * 前台公共库文件
 * 主要定义前台公共函数库
 */



/**
 * 获取设置子目录名称
 * @param  string $id 文档ID
 * @return path   目录路径
 * @author 水月居 
 */
function set_subName($user){
	
		//按学年/班号/学号/路径

		 $subName=C('CUR_YEAR');		 
		 $subName.='/bj-'.$user['bj_code'];
		 $subName.='/lid-'.$user['lid'];
  return $subName;
}
function set_saveName($user){
   $saveName=$user['st_code'].'_'.date('m');
   //$saveName= iconv('utf-8', 'gbk', $saveName);
    return $saveName;
}

/**验证是否图片文件扩展名*/

function is_img($file_ext)
{
    $file_ext= strtolower($file_ext);
    $pic_ext="png,gif,bmp,jpg";
    if(in_array($file_ext,explode(',', $pic_ext))){
        return true;
    }else{
      return false;  
    }
 }


 //获取作品文件图标或图形
 function get_zuopin_path($file_url,$file_ext){
    if(is_img($file_ext)){
        $file_path=__ROOT__ .'/Uploads/Zuopin'.$file_url;
    }else{
        $img_path= './Application/Zuopin/Static/images/filetype/'.$file_ext.'.gif';
   
        if(file_exists($img_path)){
			 $file_path=__ROOT__.'/Application/Zuopin/Static/images/filetype/'.$file_ext.'.gif';        
        }else{        
          $file_path=__ROOT__.'/Application/Zuopin/Static/images/filetype/unknown.gif';
        }       
    }
    return $file_path;
 }
