<?php
/**
 * 所属项目 学生作品展示 开源免费版.
 * 开发者: 水月居
 * 创建日期: 2017-03-27
 * 创建时间: 15:48
 * 版权所有 水月居科技工作室(www.shuiyueju.com)
 */
namespace Zuopin\Widget;

use Think\Controller;

class HomeBlockWidget extends Controller
{
    public function render()
    {
        $this->assignZuopin();
        $this->display(T('Application://Zuopin@Widget/homeblock'));
    }

    public function assignZuopin()
    {
        $num = modC('ZUOPIN_SHOW_COUNT', 4, 'Zuopin');
        $field = modC('ZUOPIN_SHOW_ORDER_FIELD', 'view_count', 'Zuopin');
        // dump($field);die;
        $order = modC('ZUOPIN_SHOW_ORDER_TYPE', 'desc', 'Zuopin');
        $cache = modC('ZUOPIN_SHOW_CACHE_TIME', 600, 'Zuopin');
        $data = S('zuopin_home_data');
        if (empty($data)) {
            $map = array('status' => 1);
            $content = D('Zuopin')->where($map)->order($field . ' ' . $order)->limit($num)->select();
             // dump($content );
            foreach ($content as &$v) {
            	$v['bj_name'] = D('JfClass')->where(array('bj_code'=>$v['bj_code']))->getField('bj_name');
                $v['user'] = query_user(array('id', 'nickname', 'space_url', 'space_link', 'avatar128', 'rank_html', 'st_name'), $v['uid']);
                
            }
            $data = $content;

            S('zuopin_home_data', $data, $cache);
        }
        // dump($data);
        unset($v);
        $this->assign('ZuopinContents', $data);
    }
} 