<?php
/**
 * Created by PhpStorm.
 * User: caipeichao
 * Date: 2018-11-12
 * Time: PM5:41
 */

namespace Admin\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;
use Admin\Builder\AdminTreeListBuilder;


class ZuopinController extends AdminController
{
    protected $zuopinModel;

    function _initialize()
    {
        $this->zuopinModel = D('Zuopin/zuopin');
        $this->classlist=M('JfClass')->field("id,bj_code,bj_name")->select();
        parent::_initialize();
    }
    public function index(){
    	$this->redirect('Zuopin/config',array('cate_id'=>2),0,'页面跳转中...');
    }
    public function config()
    {
        $admin_config = new AdminConfigBuilder();
        $data = $admin_config->handleConfig();
        $data['NEED_VERIFY'] = $data['NEED_VERIFY'] ? $data['NEED_VERIFY'] : 0;
        $data['SEND_MESSAGE']=$data['SEND_MESSAGE'] ? $data['SEND_MESSAGE'] : 0;
        $data['SEND_WEIBO'] = $data['SEND_WEIBO'] ? $data['SEND_WEIBO'] : 0;
        $data['DISPLAY_TYPE'] = $data['DISPLAY_TYPE'] ? $data['DISPLAY_TYPE'] : 'list';
        $data['ZUOPIN_SHOW_TITLE'] = $data['ZUOPIN_SHOW_TITLE'] ? $data['ZUOPIN_SHOW_TITLE'] : '最新作品';
        $data['ZUOPIN_SHOW_COUNT'] = $data['ZUOPIN_SHOW_COUNT'] ? $data['ZUOPIN_SHOW_COUNT'] : 4;
        $data['ZUOPIN_HOW_ORDER_FIELD'] = $data['ZUOPIN_SHOW_ORDER_FIELD'] ? $data['ZUOPIN_SHOW_ORDER_FIELD'] : 'view_count';
        $data['ZUOPIN_SHOW_ORDER_TYPE'] = $data['ZUOPIN_SHOW_ORDER_TYPE'] ? $data['ZUOPIN_SHOW_ORDER_TYPE'] : 'desc';
        $data['ZUOPIN_SHOW_CACHE_TIME'] = $data['ZUOPIN_SHOW_CACHE_TIME'] ? $data['ZUOPIN_SHOW_CACHE_TIME'] : '600';
        $admin_config->title('作品基本设置')
            ->keyBool('NEED_VERIFY', '投稿是否需要审核', '默认无需审核')
            ->keyBool('SEND_MESSAGE', '作品上传后发送消息"提示上传成功"', '默认发送消息')
            ->keyBool('SEND_WEIBO', '作品上传后同步推送至微博', '默认推送至微博')
            ->keyRadio('DISPLAY_TYPE', '默认展示形式', '前台列表默认以该形式展示',array('list'=>'列表','masonry'=>'瀑布流'))
            ->buttonSubmit('', '保存')->data($data);
        $admin_config->keyText('ZUOPIN_SHOW_TITLE', '标题名称', '在首页展示块的标题');
        $admin_config->keyText('ZUOPIN_SHOW_COUNT', '显示专辑的个数', '只有在网站首页模块中启用了作品展示块之后才会显示');
        $admin_config->keyRadio('ZUOPIN_SHOW_ORDER_FIELD', '排序值', '展示模块的数据排序方式', array('view_count' => '阅读数', 'reply_count' => '回复数', 'create_time' => '发表时间', 'update_time' => '更新时间'));
        $admin_config->keyRadio('ZUOPIN_SHOW_ORDER_TYPE', '排序方式', '展示模块的数据排序方式', array('desc' => '倒序，从大到小', 'asc' => '正序，从小到大'));
        $admin_config->keyText('ZUOPIN_SHOW_CACHE_TIME', '缓存时间', '默认600秒，以秒为单位');
        $admin_config->group('基本配置', 'NEED_VERIFY,DISPLAY_TYPE,SEND_MESSAGE,SEND_WEIBO')
                     ->group('首页展示配置', 'ZUOPIN_SHOW_COUNT,ZUOPIN_SHOW_TITLE,ZUOPIN_SHOW_ORDER_TYPE,ZUOPIN_SHOW_ORDER_FIELD,ZUOPIN_SHOW_CACHE_TIME');

        $admin_config->groupLocalComment('本地评论配置','zuopinContent');



        $admin_config->display();
    }

    public function zuopinlist()
    {
        //显示页面
        $builder = new AdminListBuilder();
        $bj_code=I('bj_code','201001');
        $attr['class'] = 'btn ajax-post';

        $map['bj_code'] = $bj_code;
        $data = $this->zuopinModel->where($map)->select();
         $this->assign('now_tab',ACTION_NAME);
         $this->assign('meta_title','学生作业成绩批改');
         $this->display('Zuopin@Admin/zuopinlist');
    }
    public function zuopinMark(){
    	if(IS_POST){
    		 //dump($_POST);//die;
    		 $data=I('post.data');
    		 foreach($data as $v){
    		 	$this->zuopinModel->save($v);
    		 	//dump($v);
    		 }
    		 //dump($data);die;
    	}
         $bj_code=I('bj_code','201001');
         $lid=I('title_id',0);
         $bj=M('JfClass')->field("id,bj_code,bj_name,grade,course_grade")->where(array('bj_code'=>$bj_code))->find();
         // dump($this->bj);
    	 $map['bj_code'] = $bj_code;
    	 if($lid){
    	 	$map['lid']=$lid;
    	 }else{
    	 	unset($map['lid']);
    	 }
    	 $grade=$bj['course_grade'];
    	 $title_list=M('CourseXinxijishu')->where(array('grade'=>$grade))->select();
          // dump($title_list);
         $data = $this->zuopinModel->where($map)->select();
         // dump($data);
         $this->assign('map',$map);
         $this->assign('tlist',$title_list);
         $this->assign('zuopinlist',$data );
         $this->assign('now_tab',ACTION_NAME);
         $this->assign('meta_title','学生作业成绩批改');
         $this->display('Zuopin@Admin/zuopinMark');
    }
    public function doDel(){
        $ids=I('ids');
        if(!$ids){
        	$this->error('请先选择删除的作品');
        }else{
        	$count=0;
        	foreach($ids as $id){
        		$this->zuopinModel->delete($id);
        		$count++;
        		//dump($count);
        		//dump($id);
        	}
        	$this->success('本次共删除'.$count.'条记录',U('zuopinMark'));
        	// dump($ids);
        }
        
    }
    public function studentlist(){
        $map['bj_code'] = I('bj_code','201001');
        $data= D('member')->where($map)->order('st_code asc')
        ->field('uid,bj_code,st_code,st_name,nickname,sex,score2 as score,last_login_time,last_login_ip')
        ->select();
       // dump($data);
        $this->assign('map',$map);
       
        $this->assign('datalist',$data );
        $this->assign('now_tab',ACTION_NAME);
        $this->assign('meta_title','学生作业成绩统计');
        $this->display('Zuopin@Admin/studentlist');
    }
    public function countScore(){
    	$ids=I('ids');
        if(!$ids){
        	$this->error('请先选择统计的学生', U('studentlist'),3);
        }else{
        	$count=0;
        	foreach($ids as $uid){                
                $count=$this->zuopinModel->where(array('uid'=>$uid))->count('score');
                $sum_score=$this->zuopinModel->where(array('uid'=>$uid))->sum('score'); 
                //学生作品积分固定为类型2               
                $up_data=array('score2'=>$sum_score, 'zuopin_count'=>$count);
                // dump($up_data);
                $res=D('Member')->where(array('uid' => $uid))->setField($up_data);        		
        		$count++;

        	}
            
        	$this->success('本次共更新'.$count.'名学生的作业成绩',U('studentlist'));
        	// dump($ids);
        }
    }
    public function add($id = 0, $pid = 0)
    {
        if (IS_POST) {
            if ($id != 0) {
                $issue = $this->issueModel->create();
                if ($this->issueModel->save($issue)) {
                    $this->success('编辑成功。');
                } else {
                    $this->error('编辑失败。');
                }
            } else {
                $issue = $this->issueModel->create();
                if ($this->issueModel->add($issue)) {

                    $this->success('新增成功。');
                } else {
                    $this->error('新增失败。');
                }
            }


        } else {
            $builder = new AdminConfigBuilder();
            $issues = $this->issueModel->select();
            $opt = array();
            foreach ($issues as $issue) {
                $opt[$issue['id']] = $issue['title'];
            }
            if ($id != 0) {
                $issue = $this->issueModel->find($id);
            } else {
                $issue = array('pid' => $pid, 'status' => 1);
            }


            $builder->title('新增分类')->keyId()->keyText('title', '标题')->keySelect('pid', '父分类', '选择父级分类', array('0' => '顶级分类') + $opt)
                ->keyStatus()->keyCreateTime()->keyUpdateTime()
                ->data($issue)
                ->buttonSubmit(U('Issue/add'))->buttonBack()->display();
        }

    }

    public function issueTrash($page = 1, $r = 20, $model = '')
    {
        $builder = new AdminListBuilder();
        $builder->clearTrash($model);
        //读取微博列表
        $map = array('status' => -1);
        $model = $this->issueModel;
        $list = $model->where($map)->page($page, $r)->select();
        $totalCount = $model->where($map)->count();

        //显示页面

        $builder->title('专辑回收站')
            ->setStatusUrl(U('setStatus'))->buttonRestore()->buttonClear('Issue/Issue')
            ->keyId()->keyText('title', '标题')->keyStatus()->keyCreateTime()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }

    public function operate($type = 'move', $from = 0)
    {
        $builder = new AdminConfigBuilder();
        $from = D('Issue')->find($from);

        $opt = array();
        $issues = $this->issueModel->select();
        foreach ($issues as $issue) {
            $opt[$issue['id']] = $issue['title'];
        }
        if ($type === 'move') {

            $builder->title('移动分类')->keyId()->keySelect('pid', '父分类', '选择父分类', $opt)->buttonSubmit(U('Issue/add'))->buttonBack()->data($from)->display();
        } else {

            $builder->title('合并分类')->keyId()->keySelect('toid', '合并至的分类', '选择合并至的分类', $opt)->buttonSubmit(U('Issue/doMerge'))->buttonBack()->data($from)->display();
        }

    }

    public function doMerge($id, $toid)
    {
        $effect_count = D('IssueContent')->where(array('ZUOPIN_id' => $id))->setField('ZUOPIN_id', $toid);
        D('Issue')->where(array('id' => $id))->setField('status', -1);
        $this->success('合并分类成功。共影响了' . $effect_count . '个内容。', U('issue'));
        //TODO 实现合并功能 issue
    }

    public function contents($page = 1, $r = 10)
    {
        //读取列表
        $map = array('status' => 1);
        $model = M('IssueContent');
        $list = $model->where($map)->page($page, $r)->select();
        unset($li);
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';


        $builder->title('内容管理')
            ->setStatusUrl(U('setIssueContentStatus'))->buttonDisable('', '审核不通过')->buttonDelete()
            ->keyId()->keyLink('title', '标题', 'Issue/Index/issueContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }

    public function verify($page = 1, $r = 10)
    {
        //读取列表
        $map = array('status' => 0);
        $model = M('IssueContent');
        $list = $model->where($map)->page($page, $r)->select();
        unset($li);
        $totalCount = $model->where($map)->count();

        //显示页面
        $builder = new AdminListBuilder();
        $attr['class'] = 'btn ajax-post';
        $attr['target-form'] = 'ids';


        $builder->title('审核内容')
            ->setStatusUrl(U('setIssueContentStatus'))->buttonEnable('', '审核通过')->buttonDelete()
            ->keyId()->keyLink('title', '标题', 'Issue/Index/issueContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }

    public function setIssueContentStatus()
    {
        $ids = I('ids');
        $status = I('get.status', 0, 'intval');
        $builder = new AdminListBuilder();
        if ($status == 1) {
            foreach ($ids as $id) {
                $content = D('IssueContent')->find($id);
                D('Common/Message')->sendMessage($content['uid'],$title = '专辑内容审核通知', "管理员审核通过了您发布的内容。现在可以在列表看到该内容了。",  'Issue/Index/issueContentDetail', array('id' => $id), is_login(), 2);
                /*同步微博*/
                /*  $user = query_user(array('nickname', 'space_link'), $content['uid']);
                  $weibo_content = '管理员审核通过了@' . $user['nickname'] . ' 的内容：【' . $content['title'] . '】，快去看看吧：' ."http://$_SERVER[HTTP_HOST]" .U('Issue/Index/issueContentDetail',array('id'=>$content['id']));
                  $model = D('Weibo/Weibo');
                  $model->addWeibo(is_login(), $weibo_content);*/
                /*同步微博end*/
            }

        }
        $builder->doSetStatus('IssueContent', $ids, $status);

    }

    public function contentTrash($page = 1, $r = 10, $model = '')
    {
        //读取微博列表
        $builder = new AdminListBuilder();
        $builder->clearTrash($model);
        $map = array('status' => -1);
        $model = D('IssueContent');
        $list = $model->where($map)->page($page, $r)->select();
        $totalCount = $model->where($map)->count();

        //显示页面

        $builder->title('内容回收站')
            ->setStatusUrl(U('setIssueContentStatus'))->buttonRestore()->buttonClear('IssueContent')
            ->keyId()->keyLink('title', '标题', 'Issue/Index/issueContentDetail?id=###')->keyUid()->keyCreateTime()->keyStatus()
            ->data($list)
            ->pagination($totalCount, $r)
            ->display();
    }
}
