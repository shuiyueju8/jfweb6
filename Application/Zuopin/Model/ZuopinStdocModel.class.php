<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水月居 <singliang@qq.com> <http://blog.sina.com.cn/shuiyueju8/>
// +----------------------------------------------------------------------

namespace zuopin\Model;
use Think\Model;
use Think\Upload;
use Think\Page;
/**
 * Class ZuopinModel 作品展示模型
 * @package Zuopin\Model
 * @auth 水月居
 */
class ZuopinModel extends Model{


    protected $tableName = 'zuopin_stdoc';
	
    protected $_validate = array(
        array('title', '1,100', '标题长度不合法', self::EXISTS_VALIDATE, 'length'),
        array('content', '1,40000', '内容长度不合法', self::EXISTS_VALIDATE, 'length'),
    );
     /**
     * 自动完成
     * @var array
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', 1, self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
    );

   
    /**
     * 文件上传
     * @param  array  $files   要上传的文件列表（通常是$_FILES数组）
     * @param  array  $setting 文件上传配置
     * @param  string $driver  上传驱动名称
     * @param  array  $config  上传驱动配置
     * @return array           文件上传成功后的信息
     */
    public function upload($file, $setting, $driver = 'local', $config = null){
        /* 上传文件 */
        //$setting['callback'] = array($this, 'isFile');
        $setting['removeTrash'] = array($this, 'removeTrash');
        $Upload = new Upload($setting, $driver, $config);

            $ext = strtolower($file['ext']);
            if(in_array($ext, array('jpg','jpeg','bmp','png'))){
                hook('dealPicture',$file['tmp_name']);
            }
 

        $info = $Upload->upload($files);

        if($info){ //文件上传成功，记录文件信息
            foreach ($info as $key => &$value) {
                /* 已经存在文件记录 */
                if(isset($value['id']) && is_numeric($value['id'])){
                    continue;
                }

                /* 记录文件信息 */
                if(strtolower($driver)=='sae'){
                    $value['path'] = $config['rootPath'].'Picture/'.$value['savepath'].$value['savename']; //在模板里的url路径
                }else{
                    if(strtolower($driver) != 'local'){
                        $value['path'] =$value['url'];
                    }
                    else{
                        $value['path'] = (substr($setting['rootPath'], 1).$value['savepath'].$value['savename']);   //在模板里的url路径
                    }

                }

                $value['type'] = $driver;

                if($this->create($value) && ($id = $this->add())){
                    $value['id'] = $id;
                } else {
                    //TODO: 文件上传成功，但是记录文件信息失败，需记录日志
                    unset($info[$key]);
                }
            }

            foreach($info as &$t_info){
                if($t_info['type'] =='local'){
                    $t_info['path']=get_pic_src($t_info['path']);
                }
                else{
                    $t_info['path']=$t_info['path'];
                }


            }
          /*  dump(getRootUrl());
            dump($info);
            exit;*/

            return $info; //文件上传成功
        } else {
            $this->error = $Upload->getError();
            return false;
        }
    }




    /**
     * 下载本地文件
     * @param  array    $file     文件信息数组
     * @param  callable $callback 下载回调函数，一般用于增加下载次数
     * @param  string   $args     回调函数参数
     * @return boolean            下载失败返回false
     */
    public function downLocalFile($root, $id, $callback = null, $args = null){
        
        $aFile = $this->find($id);

        $file_path=$root.$aFile['file_url'];
  
            /**处理可能存在中文名的路径*/
            $file_path_gb = iconv('utf-8', 'gbk', $file_path); //对可能出现的中文名称进行转码    
            if (file_exists($file_path)) {
              $file_path=$file_path;     
            }elseif(file_exists($file_path_gb)){
              $file_path=$file_path_gb;
            }else{
              $this->error = '不存在该文件或被删除！';
              return false;
            }

            $file_name = str_replace(' ','',$aFile['title'].'.'.$aFile['file_ext']);  //获取文件名称 basename()
            $file_size = filesize($file_path); //获取文件大小
            // dump($file_name);
            // dump($file_size);die;
        if(is_file($file_path)){
            /* 调用回调函数新增下载数 */
            // is_callable($callback) && call_user_func($callback, $args);
                        // $fp = fopen($file_path, 'r'); //以只读的方式打开文件
                        // header("Content-type: application/octet-stream");
                        // header("Accept-Ranges: bytes");
                        // header("Accept-Length: {$file_size}");
                        //         if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
                        //              header('Content-Disposition: attachment; filename="' . rawurlencode($file_name) . '"');
                        //          } else {
                        //              header('Content-Disposition: attachment; filename="' . $file_name . '"');
                        //          }
                        // //header("Content-Disposition: attachment;filename={$file_name}");
                        // $buffer = 2048;
                        // $file_count = 0;
                        // //判断文件是否结束
                        // while (!feof($fp) && ($file_size-$file_count>0)) {
                        // $file_data = fread($fp, $buffer);
                        // $file_count += $buffer;
                        // echo $file_data;
                        // }
                        // fclose($fp); //关闭文件

            /* 执行下载 */ //TODO: 大文件断点续传
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
    
            header('Content-Length:' .$file_size);
            if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
                 header('Content-Disposition: attachment; filename="' . rawurlencode($file_name) . '"');;
            } else {
                header('Content-Disposition: attachment; filename="' . $file_name . '"');
            }
            readfile($file_path);
            exit; 

        } else {
            $this->error = '文件已被删除！';
            return false;
        }
     }

    /**
     * 清除数据库存在但本地不存在的数据
     * @param $data
     */
    public function removeTrash($data){
        $this->where(array('id'=>$data['id'],))->delete();
    }

}
