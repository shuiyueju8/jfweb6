-- -----------------------------
-- 表结构 `ocenter_zuopin_comment`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_zuopin_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `zuopin_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `to_comment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_zuopin_stdoc`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_zuopin_stdoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '作业ID',
  `grade` char(2) DEFAULT NULL,
  `term` char(6) DEFAULT NULL,
  `bj_code` char(10) NOT NULL COMMENT '班级代号',
  `name` char(8) NOT NULL COMMENT '学生姓名',
  `lid` int(11) NOT NULL COMMENT '课程ID',
  `title` varchar(20) NOT NULL COMMENT '课程标题',
  `content` text NOT NULL COMMENT '作业内容',
  `keyword` varchar(200) NOT NULL COMMENT '搜索关键词',
  `file_url` varchar(150) NOT NULL COMMENT '作业文件URL',
  `file_ext` char(6) NOT NULL COMMENT '作业扩展名',
  `md5` char(32) NOT NULL COMMENT '作业文件MD5',
  `is_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `st_code` int(8) NOT NULL COMMENT '学号',
  `score` tinyint(8) NOT NULL COMMENT '成绩',
  `reply_count` int(11) NOT NULL,
  `view_count` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL COMMENT '上传日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='学生作业表';


-- -----------------------------
-- 表结构 `ocenter_zuopin_stdoc1`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_zuopin_stdoc1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '作业ID',
  `lid` int(11) NOT NULL COMMENT '课程ID',
  `ext` char(6) NOT NULL COMMENT '作业扩展名',
  `st_code` char(8) NOT NULL COMMENT '学号',
  `name` char(8) NOT NULL COMMENT '学生姓名',
  `bj_code` char(10) NOT NULL COMMENT '班级代号',
  `keti` varchar(20) NOT NULL COMMENT '课题',
  `url` varchar(80) NOT NULL COMMENT '作业文件URL',
  `is_best` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否优秀',
  `score` tinyint(1) NOT NULL COMMENT '成绩',
  `up_date` date NOT NULL COMMENT '上传日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学生作业表';

-- -----------------------------
-- 表内记录 `ocenter_zuopin_stdoc`
-- -----------------------------
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('1', '5B', '2018A', '201003', 'administ', '103', '第1课 走进计算机', '<p>作者：administrator</p>', ',administrator,[103],第1课 走进计算机', '2017/201203/6103/第1课走进计算机-88administrator.doc', 'doc', '442c85bcdf49ded7ae4f06ce06312cc6', '0', '1', '88', '20', '0', '25', '1505282457', '1505282457');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('2', '6B', '2018A', '201002', 'admin', '103', '第1课 走进计算机', '<p>作者：admin</p>', ',administrator,[103],第1课 走进计算机', '2017/201206/6103/第1课走进计算机-88administrator.docx', 'docx', 'ca89bd23ca95d61d4059eb6a649d329c', '0', '1', '88', '20', '0', '10', '1506129420', '1506129420');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('3', '6B', '2018A', '201301', 'admin', '107', '第5课 软硬兼施', '<p>作者：admin</p>', ',admin,[107],第5课 软硬兼施', '2016/201301/6107/第5课软硬兼施-admin.doc', 'doc', '710ab5fe4880922fa3352e344f44442f', '1', '1', '88', '20', '0', '13', '1542523602', '1542523602');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('4', '6B', '2018B', '201301', 'admin', '106', '第4课 安全小博士', '<p>作者：admin</p>', ',admin,[106],第4课 安全小博士', '2016/201301/6106/第4课安全小博士-admin.doc', 'doc', '710ab5fe4880922fa3352e344f44442f', '1', '1', '88', '20', '0', '0', '1542526035', '1542526035');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('5', '6B', '2018A', '201301', 'admin', '105', '第3课 小小工程师', '<p>作者：admin</p>', ',admin,[105],第3课 小小工程师', '2016/201301/6105/第3课小小工程师-admin.docx', 'docx', 'ffd4f6e0f58bfafd461ca1c7b04f4849', '0', '1', '88', '20', '0', '2', '1542721597', '1542721597');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('6', '5B', '2018A', '201301', 'admin', '107', '第5课 软硬兼施', '<p>作者：admin</p>', ',admin,[107],第5课 软硬兼施', '2016/201301/6107/第5课软硬兼施-admin.docx', 'docx', '8fd1f293912eb0dd5b1f1d31628dc4c6', '0', '1', '88', '20', '0', '0', '1542887291', '1542887291');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('7', '5B', '2018A', '201401', 'admin', '86', '第1课生活在信息中', '<p>作者：admin</p>', '五年级1班,admin,[86],第1课生活在信息中', '2018A/201401/86/第1课生活在信息中_五年级1班88admin.doc', 'doc', 'de4d300e80dcb31fff54747bb4e3f83e', '0', '1', '88', '20', '0', '2', '1555246393', '1555246393');
INSERT INTO `ocenter_zuopin_stdoc` VALUES ('8', '5B', '2018A', '201401', 'admin', '124', '第5课编制毕业留念册', '<p>作者：admin</p>', '五年级1班,admin,[124],第5课编制毕业留念册', '2018A/201401/124/第5课编制毕业留念册_五年级1班88admin.pptx', 'pptx', 'f632a56289c058769bbad79065c0c93e', '0', '1', '88', '20', '0', '1', '1555257043', '1555257043');
