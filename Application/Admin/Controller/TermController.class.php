<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/8/25
 * Time: 14:30
 */


namespace Admin\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;



class TermController extends AdminController
{


    function _initialize()
    {
        parent::_initialize();
        header("Content-Type:text/html;charset=utf-8");
        $this->yearModel=M('year');
        $this->termModel=M('yearTerm');        
    }
    public function index(){        
        /**设置自动转向 **/
        $this->redirect('setYearTerm');
    }
     
     public function editYear(){
        $id=I('id',0); 
        //判断是否在编辑模式
        $isEdit = $id ? true : false;
        if (IS_POST) {
        
        
        $id=I('id',0);

        //写入数据库
        $model = $this->yearModel;
        $data = array('year' =>I('year'), 'year_ch' =>I('year_ch','','text'),'cur'=>I('cur',0));
        if ($isEdit) {
            $result = $model->where(array('id' => $id))->save($data);
        } else {
            $result = $model->add($data);
        }
        //如果写入不成功，则报错
        if ($result === false) {
            $this->error($isEdit ? '编辑失败！' : '新增失败！');
        }
        //返回成功信息
        $this->success($isEdit ? '编辑成功！' : '新增成功！');
    } else {

    //显示页面       
    $builder = new AdminConfigBuilder();    
    //判断是否在编辑模式
    $isEdit = $id ? true : false;
    //读取学年信息
            if($isEdit){
                $list=$this->yearModel->find($id);
            }else{
                $list=array();
            }
   
    
    $builder->title($isEdit ? '学年信息编辑':'新增学年')
    
    ->keyId()->keyText('year','学年','2016')->keyText('year_ch','学年（中文）','2016学年')
    ->keyBool('cur', '当前学年', '设为当前学年')
    ->data($list) ->buttonSubmit(U('editYear'))->buttonBack()
    ->display();

    }




    }
    public function editTerm(){

        $id=I('id',0); 
        //判断是否在编辑模式
        $isEdit = $id ? true : false;
        if (IS_POST) {        $id=I('id',0);

        //写入数据库
        $model = $this->termModel;
        $data = array('term' =>I('term'), 'term_ch' =>I('term_ch','','text'));
        if ($isEdit) {
            $result = $model->where(array('id' => $id))->save($data);
        } else {
            $result = $model->add($data);
        }
        //如果写入不成功，则报错
        if ($result === false) {
            $this->error($isEdit ? '编辑失败！' : '新增失败！');
        }
        //返回成功信息
        $this->success($isEdit ? '编辑成功！' : '新增成功！');
            } else {

        //显示页面       
        $builder = new AdminConfigBuilder();    
        //判断是否在编辑模式
        $isEdit = $id ? true : false;
        //读取学年信息
            if($isEdit){
                $list=$this->yearModel->find($id);
            }else{
                $list=array();
            }
        $builder = new AdminConfigBuilder();
        
       
        $list=$this->termModel->find($id);
        $builder->title('学期信息编辑')
        
        ->keyId()->keyText('term','学期','2016')
        ->keyText('term_ch','学期（中文）','2016学期第1学期')
        ->keyText('term_chj','学期（短中文）','2016上')
        ->keyText('sort','排序')
        ->keyBool('cur', '当前学期', '设为当前学期')
        ->data($list) ->buttonSubmit(U('editTerm'))->buttonBack()
       ->display();
        }

    }
     /*学年管理*/
     public function setYear(){

       // $this->meta_title = '学年管理';
        $builder = new AdminListBuilder();
        $attr1=array(
            'class' =>'btn ajax-post btn-success'
            );
        // 记录当前列表页的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $list=$this->yearModel->order('sort asc')->select();
        $builder->title('学年管理')
        ->buttonNew(U('Term/editYear'))        
        ->buttonDeleteTrue(U('Term/deleteYear'))
        ->buttonDelete(U('Term/deleteYear'),'删除')
        ->buttonSort(U('Term/sortYear'),'排序',array('class' =>'btn btn-success'))       
        ->keyId()->keyText('sort',L('排序'))
        ->keyText('year','学期')
        ->keyText('year_ch','学期（中文）')
        ->keyBool('cur', '当前学年')
        ->keyDoAction('setcurYear?id=###','设为当前学年')
        ->keyDoActionEdit('editYear?id=###','编辑')
        ->keyDoAction('deleteYear?ids=###',"删除")
        ->data($list) 
        ->display();
        
    }
        /**
     * 学年排序
     * @author shuiyueju <singliang@163.com>
     */
    public function sortYear(){
        $model=$this->yearModel;
        if(IS_GET){
            $ids = I('get.ids');
            // $pid = I('get.pid');

            //获取排序的数据
            $map = array('status'=>array('gt',-1));
            $map['hide']=0;
            if(!empty($ids))$map['id'] = array('in',$ids);

            $list = $model->where($map)->field('id,year_ch as title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '学年排序';
            $this->subnav=U('Term/setYear');//菜单高亮
            $this->display('Term/sort');
        }elseif (IS_POST){
            $ids = I('post.ids');
            
            $ids = explode(',', $ids);
            
            foreach ($ids as $key=>$value){
                $res = $model->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！');
            }else{
                $this->error('排序失败！',U('sortYear'));
            }
        }else{
            $this->error('非法请求');
        }
    }

     /**
     * 学期排序
     * @author shuiyueju <singliang@163.com>
     */
    public function sortTerm(){
        $model=$this->termModel;
        if(IS_GET){
            $ids = I('get.ids');
            // $pid = I('get.pid');

            //获取排序的数据
            $map = array('status'=>array('gt',-1));
            //$map['hide']=0;
            if(!empty($ids))$map['id'] = array('in',$ids);

            $list = $model->where($map)->field('id,term_ch as title')->order('sort asc, id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '学年排序';
            $this->subnav=U('Term/setYearTerm');//菜单高亮
            $this->display('Term/sort');
        }elseif (IS_POST){
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key=>$value){
                $res =$model->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！');
            }else{
                $this->error('排序失败！');
            }
        }else{
            $this->error('非法请求');
        }
    }

    /*设为当前学年*/
    public function setcurYear($id){
        $aId = I('id', 0, 'intval');
        //$aId=0;
        $map['cur']=1;
        $model=$this->yearModel;
        $model->where($map)->setField(array('cur'=>0));
        $result = $model
        ->where(array('id'=>$aId))
        ->setField($map);
                //如果写入不成功，则报错
        if ($result === false) {
            $this->error($model->getError());
        }
       
        $this->success('设定成功！',U('setYear'));
    }

    /*设为当前学期*/
    public function setcurTerm($id){
        $aId = I('id', 0, 'intval');
        $map['cur']=1;
        $model=$this->termModel;
        $model->where($map)->setField(array('cur'=>0));
        $result = $model->where(array('id'=>$aId))->setField($map);
        //如果写入不成功，则报错
        if ($result === false) {
            $this->error($model->error());
        }
        $this->success('设定成功！',U('setYearTerm'));
    }

    
    /*学期管理*/
    public function setYearTerm(){
        // 记录当前列表页的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $builder = new AdminListBuilder();
        $list=M('yearTerm')->select();
        $builder->title('学期管理')
            ->buttonNew(U('Term/editTerm'))
             ->buttonDeleteTrue(U('Term/deleteTerm'))
             ->buttonDelete(U('Term/deleteTerm'))
             // ->ajaxButton(U('Term/deleteTerm'),'','删除')
             ->buttonSort(U('Term/sortTerm'))
            ->keyId()
            ->keyText('sort',L('排序'))
            ->keyText('term','学期')->keyText('term_ch','学期（中文）')
            ->keyText('term_chj','学期（短中文）')
            ->keyBool('cur', '当前学期', 'text')
            ->keyDoAction('setcurTerm?id=###','设为当前学期')
            ->keyDoActionEdit('editTerm?id=###')
            
            ->keyDoAction('Term/deleteTerm?ids=###','删除')
           
            ->data($list) 
            ->display();
    }

    /**删除学年*/
    public function deleteYear($ids){
        !is_array($ids)&&$ids=explode(',',$ids);
        $map['id'] = array('in', $ids);
        $model=$this->yearModel;
        $res = $model->where($map)->delete();
        if($res){
            $this->success('删除成功',U('Term/setYear'));
        }else{
            $this->error('删除失败'.$model->getError());
        }
    }

    /**删除学期*/
    public function deleteTerm($ids){
        !is_array($ids)&&$ids=explode(',',$ids);
        $map['id'] = array('in', $ids);
        $model=$this->termModel;
        $res = $model->where($map)->delete();
        if($res){
            $this->success('删除成功',U('Term/setYearTerm'));
        }else{
            $this->error('删除失败'.$model->getError());
        }
    }
}