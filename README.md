opensns-V6

#项目介绍
2017年9月29日更新，全面升级到V5产品介绍：www.opensns.cn业界最强大的开源SNS系统，最受社交创业项目欢迎的开源项目。
现在已经升级至V6，但BUG依旧很多，现修复了一些BUG


#软件架构
软件架构说明 ZUI1.8.1+THINKPHP3.2+JQUERY


#安装教程
添加phpexcel支持导出导入excel文件。路径为：ThinkPHP\Library\Vendor\PHPexcel\xxxxxxxx

#使用说明
增加云盘功能xxxxxxxx

#存在问题

##1. 不能备份数据库文件

找到/conf/common.php将第116行注释掉，session数据不存放在数据库即可，但相应不支持统计在线人数
// 'SESSION_TYPE'   => 'db',  //数据库存储session


##2.opensns有许多模型不存在实际的数据表模型继承了Model造成了找不到数据表的错误。

在Thinkphp中会报错如Common/Model/PushingModel.class.php，
出错信息如下“Table 'opensns5.ocenter_pushing' doesn't exist [ SQL语句 ] : SHOW COLUMNS FROM 'ocenter_pushing'”。
修复方法：去除 extends Model，无需继承Model类，ThinkPHP就不会去检测数据库中的字段，也就不会出现报错信息，这样效率也会高很多。
OpnSNS因为存在许多这样的BUG所以程序运行起来很慢。相似有weibo/Model/ShareModel.class.php,解决办法同上。


##3.存在许多函数未定义的错误，虽然不影响正常运行，但会有警示信息，运行久了Log就会变得很大，影响程序运行效率。

#参与贡献

Fork 本项目新建 Feat_xxx 分支提交代码新建 Pull Request

码云特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md码云官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解码云上的优秀开源项目
GVP 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目码云官方提供的使用手册 http://git.mydoc.io/
码云封面人物是一档用来展示码云会员风采的栏目 https://gitee.com/gitee-stars/
