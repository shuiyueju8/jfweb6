1.升级后需要在在表member中新增3个字段
  `fans` int(11) NOT NULL DEFAULT '0' COMMENT '粉丝数',
  `session_id` varchar(255) NOT NULL,
  `alive_line` int(11) NOT NULL DEFAULT '0' COMMENT '用户禁用时间至该值',
2.新增shorurl表
DROP TABLE IF EXISTS `ocenter_ucenter_short`;
CREATE TABLE `ocenter_ucenter_short` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `short` varchar(25) NOT NULL COMMENT '个性域名',
  `can_change_num` int(11) NOT NULL COMMENT '可修改次数，备用字段，用户商业或任务获得',
  `status` tinyint(4) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户个性域名';
3. 与2.8.2比较新增moudel表的men_hiden、auth字段